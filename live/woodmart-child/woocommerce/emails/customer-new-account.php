<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s: Customer username */ ?>
<p><?php printf( esc_html__( 'Kære %s,', 'woocommerce' ), esc_html( $user_login ) ); ?></p>
<?php /* translators: %1$s: Site title, %2$s: Username, %3$s: My account link */ ?>
<p><?php echo esc_html__( 'Derfor har din arbejdsgiver gjort det muligt for dig at vælge lige det du ønsker dig i år – og det gør du på hjemmesiden: www.voresjulegave.dk', 'woocommerce' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
<p><?php echo esc_html__( 'Julegaverne er lokale – og unikt udvalgt til dig.', 'woocommerce' ); ?></p>
<p>Julegaverne er lokale – og unikt udvalgt til dig.<br/>
Derfor kan du nu logge på din nye konto for at vælge en julegave på:<br/>
https://www.voresjulegave.dk/min/konto/</p>

<?php if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) && $password_generated ) : ?>
	<?php /* translators: %s: Auto generated password */ ?>
	<p><?php printf( esc_html__( 'Din adgangskode er blevet oprettet automatisk: %s', 'woocommerce' ), '<strong>' . esc_html( $user_pass ) . '</strong>' ); ?></p>
<?php endif; ?>
<p><?php echo esc_html__( 'Når du logger ind på Vores Julegave, har du mulighed for at vælge præcis den julegave, som du har ønsket dig i år. Det gør du ved at gennemse de udvalgte kategorier – og klikke på Vælg Julegave, når du har besluttet dig. Bekræft herefter dit valg og log ud igen – nemmere bliver det ikke.', 'woocommerce' ); ?></p>
<p><?php printf( esc_html__( 'Din julegave skal være valgt senest 28th september %s', 'wooCommerce' ), date_i18n( 'Y' ) ); ?></p>
<p><?php echo esc_html__( 'Tekniske problemer? Tag kontakt til virksomhedens gavekoordinater, som vil kontakte serviceteamet.', 'wooCommerce' ); ?></p>
<p>Venligst<br/>Vores Julegave</p>
<?php

do_action( 'woocommerce_email_footer', $email );
