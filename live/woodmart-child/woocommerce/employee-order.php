<?php
$orders_columns = apply_filters(
	'woocommerce_account_employee_orders_columns',
	array(
		'employee-name'  => __( 'Navn', 'woodmart' ),
		'employee-email'  => __( 'E-mail', 'woodmart' ),
		'order-complated'    => __( 'Bestilling afsluttet', 'woodmart' ),
		'price-category'  => __( 'Priskategori', 'woodmart' ),
		'price'   => __( 'Pris', 'woodmart' ),
		'order-actions' => __( 'Handlinger', 'woodmart' ),
	)
);
$employee_ids = array();
if ( is_user_logged_in() && current_user_can( 'company_coordinator' ) ) {
	$employee_ids = woodmart_get_employee_by_company( get_current_user_id() );
}

if ( $employee_ids ) : ?>

	<table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
		<thead>
			<tr>
				<?php foreach ( $orders_columns as $column_id => $column_name ) : ?>
					<th class="woocommerce-orders-table__header woocommerce-orders-table__header-<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
				<?php endforeach; ?>
			</tr>
		</thead>

		<tbody>
			<?php
			foreach ( $employee_ids as $employee_id ) {
				//$last_order = woodmart_child_get_current_year_order( $employee_id );
				$all_orders = woodmart_child_get_current_year_orders( $employee_id );
				$order_complated = ! empty( $all_orders ) ? __( 'Ja', 'woodmart' ) : __( 'Nej', 'woodmart' );
				$order_price = '-';
				$item_count = 0;
				$order_amount = 0;
				$actions_links = [];
				if ( ! empty( $all_orders ) ) {
					foreach ( $all_orders as $orders ) {
						$order = wc_get_order( $orders['id'] );
						$item_count += $order->get_item_count() - $order->get_item_count_refunded();
						$order_amount += $order->get_total();
						$actions = wc_get_account_orders_actions( $order );
						if ( ! empty( $actions ) ) {
							foreach ( $actions as $key => $action ) {
								$actions_links[] = '<a href="' . esc_url( $action['url'] ) . '"' . sanitize_html_class( $key ) . '">#' . $orders['id'] . '</a>';
							}
						}
					}
					$order_price = wp_kses_post( sprintf( _n( '%1$s for %2$s item', '%1$s for %2$s items', $item_count, 'woodmart' ), wc_price( $order_amount ), $item_count ) );
				}
				// User info.
				$customer_info = get_user_by( 'ID', $employee_id );
				$first_name = $customer_info->first_name;
				$last_name = $customer_info->last_name;
				$name = $customer_info->display_name;
				if ( ! empty( $first_name ) && ! empty( $last_name ) ) {
					$name = $first_name . ' ' . $last_name;
				}
				// Price.
				$max_price = get_user_meta( $customer_info->ID, '_max_price', true );
				if ( $max_price ) {
					$max_price = wc_price( $max_price );
				} else {
					$max_price = wc_price( 0 );
				}
				?>
				<tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-order">
					<?php foreach ( $orders_columns as $column_id => $column_name ) : ?>
						<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
							<?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
								<?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

							<?php elseif ( 'employee-name' === $column_id ) : ?>
								<?php echo esc_html( $name ); ?>

							<?php elseif ( 'employee-email' === $column_id ) : ?>
								<?php echo esc_html( $customer_info->user_email ); ?>

							<?php elseif ( 'order-complated' === $column_id ) : ?>
								<?php echo esc_html( $order_complated ); ?>

							<?php elseif ( 'price-category' === $column_id ) : ?>
								<?php echo $max_price; ?>

							<?php elseif ( 'price' === $column_id ) : ?>
								<?php echo $order_price; ?>

							<?php elseif ( 'order-actions' === $column_id ) : ?>
								<?php
								if ( ! empty( $actions_links ) ) {
									echo join( '', $actions_links );
								} else {
									echo '<a href="javascript:;" class="woocommerce-button button no-order">' . __( 'vis', 'woodmart' ) . '</a>';
								}
								?>
							<?php endif; ?>
						</td>
					<?php endforeach; ?>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>
	<?php else : ?>
	<div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
		<a class="woocommerce-Button button" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			<?php esc_html_e( 'Browse products', 'woodmart' ); ?>
		</a>
		<?php esc_html_e( 'No order has been made yet.', 'woodmart' ); ?>
	</div>
	<?php endif; ?>
