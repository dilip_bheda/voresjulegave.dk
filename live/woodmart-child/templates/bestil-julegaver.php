<?php
/**
 * Template for bestil julegaver
 *
 * Template Name: Bestil Julegaver
 * @package WordPress
 */

if ( is_user_logged_in() && ! current_user_can( 'administrator' ) ) {
	wp_redirect( home_url() );
	exit;
}

get_header(); ?>
<div class="container bestil-julegaver-container">
<div class="bestil-julegaver-form">
    <h3>
        <strong>… fordi dine medarbejdere har fortjent en unik og lokal julegave.  </strong>
    </h3>
    <p>Du er nu ved at tilmelde din virksomhed til www.voresjulegave.dk, så dine medarbejdere har mulighed for at vælge deres helt egen lokale julegave.  
 </p>
 <p>Alt du skal gøre, er at udfylde nedenstående formular med <strong>Firmanavn, Adresse, Kontaktperson (Gavekoordinater), E-mail, Telefonnummer og eventuelt EAN nummer.</strong> 
 Herefter skal du uploade en CSV-fil, hvor der er angivet medarbejdernes fulde navn, deres E-mail og den priskategori, som medarbejderen skal kunne vælge en gave fra. </p>
 <p>Herefter modtager dine medarbejdere et unikt login – og så kan de vælge deres egen personlige julegave. Som gavekoordinater får du naturligvis også et unikt koordinater-login, så du kan sikre at alle medarbejdere har valgt en julegave inden deadline. </p>
 <p><strong>Bemærk, at der kun kan vælges mellem følgende priskategorier: 200, 300, 500, 800 og 1200 kr.  
Betaling sker pr. faktura, som fremsendes til gavekoordinatoren.</strong> </p>
<p><em>*Husk også at angive gavekoordinatorens e-mail i CSV-filen, så han/hun ikke bliver glemt!</em></p>
	<form method="post" enctype="mutlipart/form-data" id="bestil_julegaver">
		<input type="hidden" name="action" value="woodmart_create_company">
		<?php wp_nonce_field( '_woodmart_blank_order', 'security_nonce' ); ?>
			<div class="wood-input-group">
			<label><?php _e( 'Firma:', 'woodmart' ); ?></label>
			<input type="text" name="company" placeholder="<?php esc_attr_e( 'Angiv firmanavn', 'woodmart' ); ?>">
		</div>
		<div class="wood-input-group">
			<label><?php _e( 'Adresse:', 'woodmart' ); ?></label>
			<input type="text" name="address" placeholder="<?php esc_attr_e( 'Angiv adresse', 'woodmart' ); ?>">
		</div>
		<div class="wood-input-group">
			<label><?php _e( 'Gavekoordinator:', 'woodmart' ); ?></label>
			<input type="text" name="contact_person" placeholder="<?php esc_attr_e( 'Angiv navn på gavekoordinator', 'woodmart' ); ?>">
		</div>
		<div class="wood-input-group">
			<label><?php _e( 'E-mail:', 'woodmart' ); ?></label>
			<input type="text" name="email" placeholder="<?php esc_attr_e( 'Angiv gavekoordinators e-mail', 'woodmart' ); ?>">
		</div>
		<div class="wood-input-group">
			<label><?php _e( 'Telefonnummer:', 'woodmart' ); ?></label>
			<input type="text" name="telephone_number" placeholder="<?php esc_attr_e( 'Angiv telefonnummer', 'woodmart' ); ?>">
		</div>
		<div class="wood-input-group">
			<label><?php _e( 'EAN nummer:', 'woodmart' ); ?></label>
			<input type="text" name="ean_number" placeholder="<?php esc_attr_e( 'Angiv EAN nummer', 'woodmart' ); ?>">
		</div>
		<div class="wood-input-group csv-notice">
		    <br>
			<p><strong><?php _e( 'Medarbejderens fulde navn, e-mail og priskategori:', 'woodmart' ); ?></strong></p>
			<div class="upload-csv">
				<input type="file" name="employee_csv" class="employee-csv" id="employee_csv">
				<span class="display-file-name" style="display: none;"></span>
			</div>
		</div>
		<div class="wood-input-group woodmart-help-notice">
			<p class="demo-csv-file"><?php echo wp_sprintf( __( 'Hent eksempel <a href="%1$s" style="font-weight:700;" download>CSV fil</a>', 'woodmart' ), 'https://www.voresjulegave.dk/wp-content/themes/woodmart-child/assets/eksempel-fil.csv' ); ?></p>
			<p class="show-total-amount" style="display: none;"></p>
			<p><?php _e( 'Bemærk, der kan kun vælges følgende priskategorier: 200, 300, 500, 800, 1200. <br/>Det er disse du skal tilføje i CSV filen.', 'woodmart' ); ?></p>
		</div>
		<div class="wood-input-group woodmart-payment-option">
			<strong><?php _e( 'Betaling', 'woodmart' ); ?></strong><br>
			<input type="radio" name="payment-mode" value="1" checked> <?php _e( 'Faktura (Tilsendes manuelt)', 'woodmart' ); ?><br>
			<input type="radio" name="payment-mode" value="2"> <?php _e( 'Betaling med kort', 'woodmart' ); ?>
		</div>
		<div class="wood-input-group woodmart-term-and-condition">
			<label>
				<input type="checkbox" class="term-and-condition" name="agree" value="1">
				<?php echo wp_sprintf( __( 'Jeg har læst og accepteret VoresJulegave.dk\'s <a href="%1$s" target="_blank">handelsbetingelser</a>', 'woodmart' ), home_url( '/handelsbetingelser/' ) ); ?>
			</label>
		</div>
		<div class="wood-input-group">
		</div>
		<div class="wood-form-submit">
			<div class="spinner">
				<div class="bounce1"></div>
				<div class="bounce2"></div>
				<div class="bounce3"></div>
			</div>
			<input type="submit" name="create_company" value="<?php esc_attr_e( 'BESTIL OG SEND', 'woodmart' ); ?>">
		</div>
		<div class="wood-resonse-notice" style="display: none;">
			<p><strong>Error:</strong></p>
		</div>
	</form>
</div>
</div>
<?php get_footer();
