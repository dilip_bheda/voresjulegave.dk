<?php
/**
 * Custom WooCommerce.
 *
 * @package WordPress
 * @subpackage WooCommerce
 */

// Remove coupon code from checkout page.
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );

/**
 * Add body class
 *
 * @param array $class Class.
 * @return array
 */
function woodmart_child_body_class( $class ) {
	if ( current_user_can( 'administrator' ) ) {
		$class[] = 'is-customer';
	}
	return $class;
}
//add_filter( 'body_class', 'woodmart_child_body_class' );

/**
 * Disable customer email notification.
 *
 * @param bool $is_enabled Enabled.
 * @param bool $email_object Email Object.
 * @return bool.
 */
function woodmart_child_disable_waitlist_new_account_emails( $is_enabled, $email_object ) {
	if ( 'woodmart_create_company' === $_POST['action'] ) {
		$is_enabled = false;
	}
	return $is_enabled;
}
add_filter( 'woocommerce_email_enabled_customer_new_account', 'woodmart_child_disable_waitlist_new_account_emails', 10, 2 );

/**
 * WooCommerce order processing email enabled OR disabled.
 *
 * @param bool   $enabled Email Status.
 * @param object $order Order data.
 * @return bool
 */
function woodmart_child_disable_emails_for_company_order( $enabled, $order ) {
	if ( $order ) {
		$employee_csv_file = get_post_meta( $order->get_id(), 'employee_csv_file', true );
		// If check is company coordinator order OR not.
		if ( ! empty( $employee_csv_file ) ) {
			$enabled = false;
		}
	}
	return $enabled;
}
add_filter( 'woocommerce_email_enabled_customer_processing_order', 'woodmart_child_disable_emails_for_company_order', 10, 2 );

/**
 * Enqueue scripts.
 */
function woodmart_child_enqueue_scripts() {
	if( is_page_template( 'templates/bestil-julegaver.php' ) ) {
		wp_enqueue_script( 'jquery-validation', get_stylesheet_directory_uri() . '/assets/js/jquery.validate.min.js', array( 'jquery' ), '', true );
		wp_enqueue_script( 'jquery-validation-methods', get_stylesheet_directory_uri() . '/assets/js/additional-methods.min.js', array( 'jquery' ), '', true );
	}
	wp_enqueue_script( 'woodmart-child-js', get_stylesheet_directory_uri() . '/assets/js/woodmart-child-custom.js', array( 'jquery' ), '', true );
	wp_localize_script( 'woodmart-child-js', 'WOOD_CHILD',
		array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
		)
	);
}
add_action( 'wp_enqueue_scripts', 'woodmart_child_enqueue_scripts' );

/**
 * Admin enqueue scripts.
 */
function woodmart_child_admin_enqueue_scripts() {
	wp_enqueue_style( 'woodmart-child-admin', get_stylesheet_directory_uri() . '/assets/admin/css/admin.css' );
	$company_product = post_exists( 'Samlet julegaver' );
	$wm_exclude_products = get_option( 'wm_exclude_products' );
	if ( $company_product && $wm_exclude_products != $company_product ) {
		update_option( 'wm_exclude_products', $company_product );
	}
}
add_action( 'admin_enqueue_scripts', 'woodmart_child_admin_enqueue_scripts' );

/**
 * WooCommerce order data.
 *
 * @param array $order_data Order data.
 * @return array.
 */
function woodmart_child_woocommerce_new_order_data( $order_data ) {
	if ( is_user_logged_in() ) {
		$last_company_order = get_user_meta( get_current_user_id(), '_last_company_order', true );
		// Force update if missing last order.
		if ( empty( $last_company_order ) ) {
			$last_company_order = woodmart_child_update_company_id( get_current_user_id() );
		}
		if ( $last_company_order ) {
			$order_data['post_parent'] = (int) $last_company_order;
		}
	}
	return $order_data;
}
add_filter( 'woocommerce_new_order_data', 'woodmart_child_woocommerce_new_order_data' );

/**
 * Custom login get complay info.
 *
 * @param string $user_login User login.
 * @param object $user User Object.
 */
function woodmart_child_customer_login( $user_login, $user ) {
	if ( in_array( 'customer', $user->roles ) ) {
		woodmart_child_update_company_id( $user->ID );
	}
}
add_action( 'wp_login', 'woodmart_child_customer_login', 10, 2 );

/**
 * Setups admin order columns.
 *
 * @param array $column Post Column.
 * @param int   $post_id Post ID.
 */
function woodmart_child_order_posts_custom_column( $column, $post_id ) {
	$suborders = woodmart_child_get_sub_orders( $post_id );
	// Order total.
	if ( 'new_order_total' === $column ) {
		$mainorder = wc_get_order( $post_id );
		$order_total = $mainorder->get_total();
		$usr_role = $mainorder->get_user()->roles;
		$usr_role = ! empty( $usr_role ) ? $usr_role : [];
		if( in_array( 'company_coordinator', $usr_role ) ) {
			// Update company order price.
			$order = wc_get_order( $post_id );
			$order_items_old = $order->get_items();
			// Delete old product from order.
			foreach ( $order_items_old as $key => $value ) {
				wc_delete_order_item( $key );
			}
			$edit_order = wc_get_order( $post_id );
			// Add product in order.
			$wm_exclude_products = get_option( 'wm_exclude_products' );
			$product_info = wc_get_product( $wm_exclude_products );
			$order_total_price = woodmart_child_get_all_employee_orders_total( $post_id );
			$product_info->set_price( strval( $order_total_price ) );
			$edit_order->add_product( $product_info, 1 );
			$edit_order->calculate_totals();
			$edit_order->save();

			$updated_order = wc_get_order( $post_id );
			$order_total_price = $updated_order->get_total();

			if ( ! empty( $order_total_price ) ) {
				echo wc_price( $order_total_price );
			} else {
				echo wc_price( 0 );
			}
		} else {
			echo wc_price( $order_total );
		}
	}
	// Sub order.
	if ( 'is_sub_order' === $column ) {
		if ( ! empty( $suborders ) ) {
			$counter = 1;
			$suborders_list = array();
			foreach ( $suborders as $suborder_id ) {
				$suborder = wc_get_order( $suborder_id );
				$total_spent_amount[] = $suborder->get_total();
				if ( $suborder ) {
					$suborders_list[] = '<a style="font-size:12px !important;" href="' . admin_url( 'post.php?post=' . absint( $suborder_id ) . '&action=edit' ) . '" class="row-title"><strong>#' . $suborder->get_order_number() . '</strong></a>';
					$counter ++;
				}
			}
			if ( ! empty( $suborders_list ) ) {
				echo '<div class="sub-order-list" style="max-height: 4.5em; overflow-y: auto;">';
				echo join( ', ', $suborders_list );
			}
		} else {
			echo '—';
		}
	}

	// Total Spent Amount.
	if ( 'amount_spent' === $column ) {
		$total_spent_amount = [];
		if ( ! empty( $suborders ) ) {
			foreach ( $suborders as $suborder_id ) {
				$suborder = wc_get_order( $suborder_id );
				$total_spent_amount[] = $suborder->get_total();
			}
		}
		if ( ! empty( $total_spent_amount ) ) {
			$total_spent_amount = array_sum( $total_spent_amount );
			echo wc_price( $total_spent_amount );
		} else {
			echo wc_price( 0 );
		}
	}
}
add_action( 'manage_shop_order_posts_custom_column', 'woodmart_child_order_posts_custom_column', 20, 2 );


/**
 * Adds 'Sub Order' column header to 'Orders' page immediately after 'Total' column.
 *
 * @param string[] $columns Post columns.
 * @return string[] $new_columns Post columns.
 */
function woodmart_child_order_posts_columns( $columns ) {

	$new_columns = array();

	$order_total = $columns['order_total'];
	unset( $columns['order_total'] );
	foreach ( $columns as $column_name => $column_info ) {

		$new_columns[ $column_name ] = $column_info;

		if ( 'order_status' === $column_name ) {
			$new_columns['is_sub_order'] = __( 'Sub Order', 'woodmart' );
			$new_columns['new_order_total'] = $order_total;
			$new_columns['amount_spent'] = __( 'Brugt beløb', 'woodmart' );
		}
	}

	return $new_columns;
}
add_filter( 'manage_edit-shop_order_columns', 'woodmart_child_order_posts_columns', 20 );

/**
 * Exclude products for customer.
 *
 * @param object $query Woo Product Query.
 */
function woodmart_child_woocommerce_product_query( $query ) {
	if ( is_user_logged_in() ) {
		$user_id = get_current_user_id();
		$max_price = get_user_meta( $user_id, '_max_price', true );
		if ( ! empty( $max_price ) ) {
			$all_orders = woodmart_child_get_current_year_orders( $user_id );
			$total = 0;
			if ( ! empty( $all_orders ) ) {
				$all_order_price = array_column( $all_orders, 'price' );
				$total += array_sum( $all_order_price );
			}
			$total_amount = abs( $max_price - $total );
			if ( ! empty( $max_price ) ) {
				$meta_query = array(
					array(
						'key' => '_price',
						'value' => array( 1, $total_amount ),
						'compare' => 'BETWEEN',
						'type' => 'NUMERIC',
					),
				);
				$query->set( 'meta_query', $meta_query );
			}
		}
	}
	$wm_exclude_products = get_option( 'wm_exclude_products' );
	if ( $wm_exclude_products ) {
		$query->set( 'post__not_in', [ $wm_exclude_products ] );
	}
}
add_action( 'woocommerce_product_query', 'woodmart_child_woocommerce_product_query' );

/**
 * Fire employee login notification.
 *
 * @param int $order_id Order ID.
 */
function woodmart_child_woocommerce_order_status_processing( $order_id ) {
	$order = new WC_Order( $order_id );
	$employee_csv_file = get_post_meta( $order->get_id(), 'employee_csv_file', true );
	$import_employee_status = array();
	if ( $employee_csv_file ) {
		$employee_csv_file = get_attached_file( $employee_csv_file );
		$employee_csv_file = woodmart_child_csv_to_array( $employee_csv_file );
		$customer = $order->get_user();
		if ( in_array( 'company_coordinator', $customer->roles ) ) {
			if ( ! empty( $employee_csv_file ) ) {
				$company_id = $customer->ID;
				$company_name = get_user_meta( $customer_id, 'billing_company', true );
				$import_employee_status['total'] = count( $employee_csv_file );
				foreach ( $employee_csv_file as $employee ) {
					$name = isset( $employee[0] ) && ! empty( $employee[0] ) ? $employee[0] : '';
					$email = isset( $employee[1] ) && ! empty( $employee[1] ) ? $employee[1] : '';
					$price = isset( $employee[2] ) && ! empty( $employee[2] ) ? $employee[2] : '';
					$username = str_replace( '-', '', sanitize_title( $name ) );
					if ( ! email_exists( $email ) ) {
						$user_id = wc_create_new_customer( $email, $username, '' );
						$name = explode( ' ', $name );
						if ( isset( $name[0] ) ) {
							update_user_meta( $user_id, 'first_name', $name[0] );
							update_user_meta( $user_id, 'billing_first_name', $name[0] );
						}
						if ( isset( $name[1] ) ) {
							update_user_meta( $user_id, 'last_name', $name[1] );
							update_user_meta( $user_id, 'billing_last_name', $name[1] );
						}
						if ( ! empty( $company_name ) ) {
							update_user_meta( $user_id, 'billing_company', $company_name );
						}
						update_user_meta( $user_id, '_max_price', $price );
						update_user_meta( $user_id, '_company_id', $company_id );
					} else {
						$company_info = get_user_by( 'email', $email );
						if ( in_array( 'company_coordinator', $company_info->roles ) ) {
							update_user_meta( $company_info->ID, '_max_price', $price );
						} else {
							$import_employee_status['faild'][] = array(
								'name' => $name,
								'email' => $email,
								'price' => $price,
							);
						}
					}
				}
				update_post_meta( $order_id, '_import_employees', $import_employee_status );
			}
		}
	}
}
add_action( 'woocommerce_order_status_processing', 'woodmart_child_woocommerce_order_status_processing' );

/**
 * WooCommerce order metabox.
 */
function woodmart_child_woocommerce_remove_order_item() {
	global $post;
	if ( $post && 'shop_order' === get_post_type( $post ) ) {
		$order = new WC_Order( $post->ID );
		$user_info = get_user_by( 'ID', $order->get_user_id() );
		if ( in_array( 'company_coordinator', $user_info->roles ) ) {
			//remove_meta_box( 'woocommerce-order-items', 'shop_order', 'normal' );
			add_meta_box( 'company-employee-list', __( 'Medarbejderens', 'woodmart' ), 'woodmart_child_woocommerce_employee_list', 'shop_order' );
			add_meta_box( 'import-employee-status', __( 'Importer medarbejderstatus', 'woodmart' ), 'woodmart_child_woocommerce_import_employee_status', 'shop_order' );
		}
	}
}
add_action( 'add_meta_boxes', 'woodmart_child_woocommerce_remove_order_item', 99 );

/**
 * Import employee status.
 */
function woodmart_child_woocommerce_import_employee_status() {
	global $post;
	$total_faild = 0;
	if ( $post ) {
		$import_employees = get_post_meta( $post->ID, '_import_employees', true );
		if ( ! empty( $import_employees) ) {
			$total_faild = isset( $import_employees['faild'] ) ? count( $import_employees['faild'] ) : 0;
			$position = $total_faild ? 'right' : 'left';
			echo wp_sprintf( __( '<div style="text-align: %3$s;margin-top: 15px;"><strong>Total Import: %1$s/%2$s</strong></div>', 'woodmart' ), $import_employees['total'] - $total_faild, $import_employees['total'], $position );
		}
	}
	if ( $total_faild > 0 ) :
		$faild_count = 1;
	?>
		<div>
			<h3><?php _e( 'Import Faild List', 'woodmart' ); ?></h3>	
		</div>
		<table class="wp-list-table widefat fixed striped posts employee-list">
			<thead>
				<tr>
					<th>#</th>
					<th><?php _e( 'Name', 'woodmart' ); ?></th>
					<th><?php _e( 'Email', 'woodmart' ); ?></th>
					<th><?php _e( 'Price', 'woodmart' ); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ( $import_employees['faild'] as $faild ) : ?>
					<tr>
						<td><?php echo $faild_count; ?></td>
						<td><?php echo $faild['name']; ?></td>
						<td><?php echo $faild['email']; ?></td>
						<td><?php echo wc_price( $faild['price'] ); ?></td>
					</tr>
				<?php $faild_count++; endforeach; ?>
			</tbody>
		</table>
	<?php
	endif;
}

/**
 * Update order dynamically.
 */
function woodmart_child_edit_order_page() {
	global $post;
	$order_id = isset( $_GET['post'] ) ? $_GET['post'] : $post->ID;
	if ( 'shop_order' === get_post_type( $order_id ) ) {

		$order = wc_get_order( $order_id );
		$usr_role = $order->get_user()->roles;
		
		if( in_array( 'company_coordinator', $usr_role ) ){
			$order_items_old = $order->get_items();
			// Delete old product from order.
			foreach ( $order_items_old as $key => $value ) {
				wc_delete_order_item( $key );
			}
			$edit_order = wc_get_order( $order_id );
			// Add product in order.
			$wm_exclude_products = get_option( 'wm_exclude_products' );
			$product_info = wc_get_product( $wm_exclude_products );
			$order_total_price = woodmart_child_get_all_employee_orders_total( $order_id );
			$product_info->set_price( strval( $order_total_price ) );
			$edit_order->add_product( $product_info, 1 );
			$edit_order->calculate_totals();
			$edit_order->save();
		}
	}
}
add_action( 'load-post.php', 'woodmart_child_edit_order_page' );

/**
 * Display emplyolee list.
 */
function woodmart_child_woocommerce_employee_list() {
	global $post;
	if ( $post ) :
		$order = new WC_Order( $post->ID );
		$company = woodmart_get_employee_by_company( $order->get_user_id() );
		$company[] = $order->get_user_id();
	if ( ! empty( $company ) ) :
	$count = 1;
	?>
	<table class="wp-list-table widefat fixed striped posts employee-list">
		<thead>
			<tr>
				<th>#</th>
				<th><?php _e( 'Name', 'woodmart' ); ?></th>
				<th><?php _e( 'Email', 'woodmart' ); ?></th>
				<th><?php _e( 'Price', 'woodmart' ); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ( $company as $e ) :
				$customer_info = get_user_by( 'ID', $e );
				$first_name = $customer_info->first_name;
				$last_name = $customer_info->last_name;
				$name = $customer_info->display_name;
				if ( $last_name ) {
					$name = $first_name . ' ' . $last_name;
				}
				$price = get_user_meta( $e, '_max_price', true );
			?>
			<tr>
				<td><?php echo $count; ?></td>
				<td><?php echo $name; ?></td>
				<td><?php echo $customer_info->user_email; ?></td>
				<td><?php echo wc_price( $price ); ?></td>
			</tr>
			<?php $count++; endforeach; ?>
		</tbody>
	</table>
	<?php
	endif;
	endif;
}

/**
 * Register new endpoint to use for My Account page.
 */
function woodmart_add_employee_order_endpoint() {
	add_rewrite_endpoint( 'medarbejder-ordre', EP_ROOT | EP_PAGES );
}
add_action( 'init', 'woodmart_add_employee_order_endpoint' );

/**
 * Add query var for emploree orders.
 *
 * @param array $vars Query var.
 * @return array.
 */
function woodmart_employee_order_query_vars( $vars ) {
	$vars[] = 'medarbejder-ordre';
	return $vars;
}
add_filter( 'query_vars', 'woodmart_employee_order_query_vars', 0 );

/**
 * WooCommerce account menu items.
 *
 * @param array $items Menu items.
 * @param array $endpoints Items endpoints.
 * @return array.
 */
function woodmart_child_woocommerce_account_menu_items( $items, $endpoints ) {
	$new_items = array(
		'dashboard' => $items['dashboard'],
		'orders' => $items['orders'],
		'medarbejder-ordre' => __( 'Medarbejder ordre', 'woodmart' ),
		'downloads' => $items['downloads'],
		'edit-address' => $items['edit-address'],
		'edit-account' => $items['edit-account'],
		'customer-logout' => $items['customer-logout'],
	);
	if ( ! current_user_can( 'company_coordinator' ) ) {
		unset( $new_items['medarbejder-ordre'] );
	}
	return $new_items;
}
add_filter( 'woocommerce_account_menu_items', 'woodmart_child_woocommerce_account_menu_items', 10, 2 );

/**
 * Custom employee order endpoint.
 */
function woodmart_employee_order_content() {
	ob_start();
	wc_get_template( 'employee-order.php' );
	echo ob_get_clean();
}
add_action( 'woocommerce_account_medarbejder-ordre_endpoint', 'woodmart_employee_order_content' );

/**
 * Checks if a user has a certain capability.
 *
 * @param array $allcaps All capabilities.
 * @param array $caps    Capabilities.
 * @param array $args    Arguments.
 *
 * @return array The filtered array of all capabilities.
 */
function woodmart_child_woo_user_has_cap( $allcaps, $caps, $args ) {
	if ( isset( $caps[0] ) ) {
		switch ( $caps[0] ) {
			case 'view_order':
				$user_id = intval( $args[1] );
				$order   = wc_get_order( $args[2] );
				$customer_id = $order->get_user_id();
				$company_id = get_user_meta( $customer_id, '_company_id', true );
				$company_id = $company_id ? ( int ) $company_id : 0;
				if ( $order && ( $user_id === $customer_id || $company_id === $user_id ) ) {
					$allcaps['view_order'] = true;
				}
				break;
			case 'pay_for_order':
				$user_id  = intval( $args[1] );
				$order_id = isset( $args[2] ) ? $args[2] : null;

				// When no order ID, we assume it's a new order
				// and thus, customer can pay for it.
				if ( ! $order_id ) {
					$allcaps['pay_for_order'] = true;
					break;
				}

				$order = wc_get_order( $order_id );

				if ( $order && ( $user_id === $order->get_user_id() || ! $order->get_user_id() ) ) {
					$allcaps['pay_for_order'] = true;
				}
				break;
			case 'order_again':
				$user_id = intval( $args[1] );
				$order   = wc_get_order( $args[2] );

				if ( $order && $user_id === $order->get_user_id() ) {
					$allcaps['order_again'] = true;
				}
				break;
			case 'cancel_order':
				$user_id = intval( $args[1] );
				$order   = wc_get_order( $args[2] );

				if ( $order && $user_id === $order->get_user_id() ) {
					$allcaps['cancel_order'] = true;
				}
				break;
			case 'download_file':
				$user_id  = intval( $args[1] );
				$download = $args[2];

				if ( $download && $user_id === $download->get_user_id() ) {
					$allcaps['download_file'] = true;
				}
				break;
		}
	}
	return $allcaps;
}
add_filter( 'user_has_cap', 'woodmart_child_woo_user_has_cap', 10, 3 );

/**
 * Get account menu item classes.
 *
 * @param string $classes  Menu item class.
 * @param string $endpoint Endpoint.
 * @return string
 */
function woodmart_child_woocommerce_account_menu_item_classes( $classes, $endpoint ) {
	global $wp;
	// Set current item class.
	$current = isset( $wp->query_vars[ $endpoint ] );
	$order = new WC_Order( $wp->query_vars['view-order'] );
	if ( 'orders' === $endpoint && $order->parent_id ) {
		$find_activate_class = array_search( 'is-active', $classes );
		if ( false != $find_activate_class ) {
			unset( $classes[ $find_activate_class ] );
		}
	}
	if ( $order->parent_id ) {
		if ( 'medarbejder-ordre' === $endpoint && isset( $wp->query_vars['view-order'] ) ) {
			$classes[] = 'is-active';
		}
	}
	return $classes;
}
add_filter( 'woocommerce_account_menu_item_classes', 'woodmart_child_woocommerce_account_menu_item_classes', 10, 2 );

/**
 * If check is not company.
 */
function woodmart_is_not_company() {
	global $wp;
	if ( ! current_user_can( 'company_coordinator' ) && isset( $wp->query_vars['medarbejder-ordre'] ) ) {
		wp_redirect( wc_get_account_endpoint_url( 'orders' ) );
		exit;
	}
	// Redirect to employee order table.
	/*if ( current_user_can( 'company_coordinator' ) && isset( $wp->query_vars['orders'] ) ) {
		wp_redirect( wc_get_account_endpoint_url( 'medarbejder-ordre' ) );
		exit;
	}*/
}
add_action( 'template_redirect', 'woodmart_is_not_company' );

/**
 * Overwrite page title for `Employee Order` page.
 *
 * @param string $title Post title.
 * @param int    $id Post ID.
 * @return string
 */
function woodmart_child_overwrite_title( $title, $id ) {
	global $wp;
	if ( 'my-account/medarbejder-ordre' === $wp->request && in_the_loop() ) {
		return __( 'medarbejder ordre', 'woodmart' );
	} else {
		return $title;
	}
}
//add_filter( 'the_title', 'woodmart_child_overwrite_title', 10, 2 );

/**
 * Verify CSV data.
 *
 * @param array $csv_data CSV file data.
 * @return string
 */
function woodmart_child_csv_data_is_valid( $csv_data = [] ) {
	if ( ! empty( $csv_data ) ) {
		$emails = [];
		$price = [];
		foreach ( $csv_data as $data ) {
			$emails[] = isset( $data[1] ) ? $data[1] : '';
			$price[] = isset( $data[2] ) ? $data[2] : '';
		}
		// filter all email IDs.
		$emails = array_map( function( $e ) {
			return ! is_email( $e ) ? true : false;
		}, $emails );
		// Filter all price.
		$price = array_map( function( $p ) {
			return ! is_numeric( $p ) || ! in_array( $p, [ 200, 300, 500, 800, 1200 ] ) ? true : false;
		}, $price );
		// Remove empty data from array.
		$emails = array_filter( $emails );
		$price = array_filter( $price );
		if ( ! empty( $emails ) ) {
			return 'Fundet den forkerte e-mail i CSV-filen.';
		} elseif ( ! empty( $price ) ) {
			return 'Fundet den forkerte pris i CSV-filen.';
		}
	}
	return '';
}

/**
 * Create company & blank order.
 */
function woodmart_child_create_company() {
	// AJAX verify nonce.
	check_ajax_referer( '_woodmart_blank_order', 'security_nonce' );
	$resonse = array(
		'result' => 0,
	);
	// Get postdata.
	$company = isset( $_POST['company'] ) ? sanitize_text_field( $_POST['company'] ) : '';
	$address = isset( $_POST['address'] ) ? sanitize_text_field( $_POST['address'] ) : '';
	$contact_person = isset( $_POST['contact_person'] ) ? sanitize_text_field( $_POST['contact_person'] ) : '';
	$telephone_number = isset( $_POST['telephone_number'] ) ? sanitize_text_field( $_POST['telephone_number'] ) : '';
	$email = isset( $_POST['email'] ) ? sanitize_email( $_POST['email'] ) : '';
	$ean_number = isset( $_POST['ean_number'] ) ? sanitize_text_field( $_POST['ean_number'] ) : '';

	// Verify CSV file data.
	$csv_data = woodmart_child_csv_to_array( $_FILES['employee_csv']['tmp_name'] );
	$is_valid_data = woodmart_child_csv_data_is_valid( $csv_data );

	if ( empty( $is_valid_data ) ) {
		if ( ! email_exists( $email ) ) {
			$username = sanitize_title( $company );
			// Create new company.
			$password = wp_generate_password();
			$user_id = wp_create_user( $username, $password, $email );
			if ( $user_id ) {
				$company_info = new WP_User( $user_id );
				$company_info->set_role( 'company_coordinator' );
				update_user_meta( $user_id, 'first_name', $company );
				update_user_meta( $user_id, 'billing_first_name', $company );
				update_user_meta( $user_id, 'billing_company', $company );
				update_user_meta( $user_id, 'billing_address_1', $address );
				update_user_meta( $user_id, 'billing_phone', $telephone_number );
				update_user_meta( $user_id, 'contact_person', $contact_person );
				update_user_meta( $user_id, 'ean_number', $ean_number );
				if ( ! empty( $_FILES ) ) {
					$address = array(
						'first_name' => $company,
						'company'    => $company,
						'email'      => $email,
						'phone'      => $telephone_number,
						'address_1'  => $address,
					);
					$attachment_id = woodmart_child_upload_employee_csv_file( $_FILES, $user_id );
					$order = wc_create_order(
						array(
							'customer_id' => $user_id,
							'status' => 'pending',
						)
					);
					$wm_exclude_products = get_option( 'wm_exclude_products' );
					//update final order date
					$year = date("Y");
					update_post_meta( $order->get_id(), 'final_order_date', 'November 14, '.$year.'' );

					$order->add_product( wc_get_product( $wm_exclude_products ), 1 );
					$order->set_address( $address, 'billing' );
					$order->set_address( $address, 'shipping' );
					$order->update_meta_data( 'employee_csv_file', $attachment_id );
					$order->calculate_totals();
					// Send user notification.
					woodmart_child_send_company_notification( $email, $username, $password, $order );
				}
				$resonse['result'] = 1;
				$resonse['message'] = __( 'Du har nu registreret dig og vi har modtaget din ordre. Du vil få en velkomstmail som bekræftelse.', 'woodmart' );
			}
		} else {
			$resonse['message'] = __( 'Denne email er allerede registreret.', 'woodmart' );
		}
	} else {
		$resonse['message'] = $is_valid_data;
	}
	echo json_encode( $resonse, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
	exit;
}
add_action( 'wp_ajax_woodmart_create_company', 'woodmart_child_create_company' );
add_action( 'wp_ajax_nopriv_woodmart_create_company', 'woodmart_child_create_company' );

/**
 *  WooCommerce add to cart price validation.
 *
 * @param bool $passed Is valid OR not.
 * @return bool
 */
function woodmart_child_add_to_cart_price_validation( $passed, $product_id, $quantity ) {
	if ( is_user_logged_in() ) {
		$user_id = get_current_user_id();
		$user_max_price = get_user_meta( $user_id, '_max_price', true );
		if ( ! empty( $user_max_price ) ) {
			$product = wc_get_product( $product_id );
			$total = WC()->cart->total + $product->get_price();
			$all_orders = woodmart_child_get_current_year_orders( $user_id );
			if ( ! empty( $all_orders ) ) {
				$all_order_price = array_column( $all_orders, 'price' );
				$total += array_sum( $all_order_price );
			}
			$total_amount = $user_max_price - $total;
			if ( ! empty( $total ) && $total > $user_max_price ) {
				$passed = false;
				$total -= $product->get_price();
				$total_amount = $user_max_price - $total;
			}
			if ( $total_amount < 0 ) {
				$passed = false;
				$total -= $product->get_price();
				$total_amount = $user_max_price - $total;
			}
			$total_amount = wc_price( $total_amount );
			$total_amount = str_replace( 'DKK', '', $total_amount );
			$total_amount = str_replace( '&nbsp;', '', $total_amount );
			wc_add_notice( wp_sprintf( __( 'Du har %1$s,- kr tilbage at købe gaver for.', 'woodmart' ), $total_amount ), 'error' );
		}
	}
	return $passed;
}
add_filter( 'woocommerce_add_to_cart_validation', 'woodmart_child_add_to_cart_price_validation', 10, 3 );

/**
 * Removes Checkout Fields.
 *
 * @param array $fields Checkout Fields.
 * @return array.
 */
function woodmart_child_woocommerce_checkout_fields( $fields ) {
	unset( $fields['billing']['billing_company'] );
	unset( $fields['billing']['billing_address_1'] );
	unset( $fields['billing']['billing_address_2'] );
	unset( $fields['billing']['billing_city'] );
	unset( $fields['billing']['billing_postcode'] );
	unset( $fields['billing']['billing_country'] );
	unset( $fields['billing']['billing_state'] );
	unset( $fields['billing']['billing_phone'] );
	unset( $fields['order']['order_comments'] );
	return $fields;
}
add_filter( 'woocommerce_checkout_fields', 'woodmart_child_woocommerce_checkout_fields' );

/**
 *  WooCommerce Update Cart validation.
 *
 * @param bool   $passed Is valid OR not.
 * @param string $cart_item_key Cart item key.
 * @param mixed  $values Cart value.
 * @param int    $quantity Cart quantity.
 * @return bool
 */
function woodmart_child_update_cart_price_validation( $passed, $cart_item_key, $values, $quantity ) {
	if ( is_user_logged_in() ) {
		$user_id = get_current_user_id();
		$user_max_price = get_user_meta( $user_id, '_max_price', true );
		if ( ! empty( $user_max_price ) ) {
			$total = WC()->cart->total + $values['data']->get_price();
			$all_orders = woodmart_child_get_current_year_orders( $user_id );
			if ( ! empty( $all_orders ) ) {
				$all_order_price = array_column( $all_orders, 'price' );
				$total += array_sum( $all_order_price );
			}
			$total_amount = $user_max_price - $total;
			$total_amount = $total_amount <= 0 ? 0 : $total_amount;
			if ( $quantity > WC()->cart->cart_contents_count ) {
				if ( ! empty( $total ) && $total > $user_max_price ) {
					$passed = false;
					$total -= $values['data']->get_price();
					$total_amount = $user_max_price - $total;
				}
				if ( $total_amount < 0 ) {
					$passed = false;
					$total -= $values['data']->get_price();
					$total_amount = $user_max_price - $total;
				}
			} else {
				if ( $quantity < WC()->cart->cart_contents_count ) {
					$total -= $values['data']->get_price();
					$total_amount = $user_max_price - $total + $values['data']->get_price();
					$passed = true;
				}
			}
			$total_amount = wc_price( $total_amount );
			$total_amount = str_replace( 'DKK', '', $total_amount );
			$total_amount = str_replace( '&nbsp;', '', $total_amount );
			wc_add_notice( wp_sprintf( __( 'Du har %1$s,- kr tilbage at købe gaver for.', 'woodmart' ), $total_amount ), 'error' );
		}
	}
	return $passed;
}
add_filter( 'woocommerce_update_cart_validation', 'woodmart_child_update_cart_price_validation', 10, 4 );

/**
 * WooCommerce login after redirect to page.
 *
 * @param string $redirect Redirect Page URL.
 * @param object $user User Info.
 * @return string Redirect Page.
 */
function woodmart_child_woocommerce_login_redirect( $redirect, $user ) {
	if ( in_array( 'customer', $user->roles ) ) {
		return wc_get_page_permalink( 'shop' );
	}
	return $redirect;
}
add_action( 'woocommerce_login_redirect', 'woodmart_child_woocommerce_login_redirect', 10, 2 );

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */
function woodmart_child_wp_login_redirect( $redirect_to, $request, $user ) {
	// Is there a user to check?.
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		// Check for admins.
		if ( in_array( 'customer', $user->roles ) ) {
			// redirect them to the default place.
			return wc_get_page_permalink( 'shop' );
		} else {
			return $redirect_to;
		}
	} else {
		return $redirect_to;
	}
}
add_filter( 'login_redirect', 'woodmart_child_wp_login_redirect', 10, 3 );

/**
 * Add notice header after.
 */
function woodmart_child_after_setup1_theme() {
	if ( is_user_logged_in() && ( current_user_can( 'customer' ) || current_user_can( 'company_coordinator' ) ) ) {
		remove_action( 'woodmart_after_header', 'woodmart_page_title', 10 );
		add_action( 'woodmart_after_header', 'woodmart_child_page_title', 99 );
	}
}
add_action( 'after_setup_theme', 'woodmart_child_after_setup1_theme' );

/**
 * Order details after hook.
 *
 * @param object order Order Info.
 */
function woodmart_child_order_details_after_order_table( $order ) {
	$csv_link = $order->get_meta( 'employee_csv_file' );
	$csv_link = wp_get_attachment_url( $csv_link );
	$csv_link = $csv_link ? esc_url( $csv_link ) : 'javascript:;';
	if ( current_user_can( 'company_coordinator' ) ) :
	?>
	<div class="view-csv_data">
		<a href="<?php echo $csv_link; ?>" class="woocommerce-button button view" download><?php _e( 'Medarbejder fil', 'woodmart' ); ?></a>
	</div>
	<?php endif;
}
add_action( 'woocommerce_order_details_after_order_table', 'woodmart_child_order_details_after_order_table' );

/**
 * Theme page title.
 */
function woodmart_child_page_title() {
	global $wp_query, $post;

        // Remove page title for dokan store list page

        if( function_exists( 'dokan_is_store_page' )  && dokan_is_store_page() ) {
        	return '';
        }

		$page_id = 0;

		$disable     = false;
		$page_title  = true;
		$breadcrumbs = woodmart_get_opt( 'breadcrumbs' );

		$image = '';

		$style = '';

		$page_for_posts    = get_option( 'page_for_posts' );
		$page_for_shop     = get_option( 'woocommerce_shop_page_id' );
		$page_for_projects = woodmart_tpl2id( 'portfolio.php' );

		$title_class = 'page-title-';

		$title_color = $title_type = $title_size = 'default';

		// Get default styles from Options Panel
		$title_design = woodmart_get_opt( 'page-title-design' );

		$title_size = woodmart_get_opt( 'page-title-size' );

		$title_color = woodmart_get_opt( 'page-title-color' );

		$shop_title = woodmart_get_opt( 'shop_title' );
		
		$shop_categories = woodmart_get_opt( 'shop_categories' );

		$single_post_design = woodmart_get_opt( 'single_post_design' );

		// Set here page ID. Will be used to get custom value from metabox of specific PAGE | BLOG PAGE | SHOP PAGE.
		$page_id = woodmart_page_ID();

		if( $page_id != 0 ) {
			// Get meta value for specific page id
			$disable = get_post_meta( $page_id, '_woodmart_title_off', true );

			$image = get_post_meta( $page_id, '_woodmart_title_image', true );

			$custom_title_color = get_post_meta( $page_id, '_woodmart_title_color', true );
			$custom_title_bg_color = get_post_meta( $page_id, '_woodmart_title_bg_color', true );


			if( $image != '' ) {
				$style .= "background-image: url(" . $image . ");";
			}

			if( $custom_title_bg_color != '' ) {
				$style .= "background-color: " . $custom_title_bg_color . ";";
			}

			if( $custom_title_color != '' && $custom_title_color != 'default' ) {
				$title_color = $custom_title_color;
			}
		}

		if ( $title_design == 'disable' ) $page_title = false;

		if ( ! $page_title && ! $breadcrumbs ) $disable = true;

		if ( is_single() && $single_post_design == 'large_image' ) $disable = false;

		if ( $disable ) return;

		$title_class .= $title_type;
		$title_class .= ' title-size-'  . $title_size;
		$title_class .= ' title-design-' . $title_design;

		if ( $single_post_design == 'large_image' && is_single() ) {
			$title_class .= ' color-scheme-light';
		}else{
			$title_class .= ' color-scheme-' . $title_color;
		}

		if ( $single_post_design == 'large_image' && is_singular( 'post' ) ) {
			$image_url = get_the_post_thumbnail_url( $page_id );
			if ( $image_url && ! $style ) $style .= "background-image: url(" . $image_url . ");";
			$title_class .= ' post-title-large-image';

			?>
				<div class="page-title <?php echo esc_attr( $title_class ); ?>" style="<?php echo esc_attr( $style ); ?>">
					<div class="container">
						<header class="entry-header">
							<?php if ( get_the_category_list( ', ' ) ): ?>
								<div class="meta-post-categories"><?php echo get_the_category_list( ', ' ); ?></div>
							<?php endif ?>

							<h1 class="entry-title"><?php the_title(); ?></h1>

							<div class="entry-meta woodmart-entry-meta">
								<?php woodmart_post_meta(array(
									'labels' => 1,
									'author' => 1,
									'author_ava' => 1,
									'date' => 1,
									'edit' => 0,
									'comments' => 1,
									'short_labels' => 0
								)); ?>
							</div>
						</header>
					</div>
				</div>
			<?php
			return;
		}
			
		// Heading for pages
		if( is_singular( 'page' ) && ( ! $page_for_posts || ! is_page( $page_for_posts ) ) ):
			$title = get_the_title();

			?>
				<div class="page-title <?php echo esc_attr( $title_class ); ?>" style="<?php echo esc_attr( $style ); ?>">
					<div class="container">
						<header class="entry-header">
							<?php if ( woodmart_woocommerce_installed() && ( is_cart() || is_checkout() ) ): ?>
								<?php woodmart_checkout_steps(); ?>
							<?php else: ?>
								<?php if( $page_title ): ?><h1 class="entry-title"><?php echo esc_html( $title ); ?></h1><?php endif; ?>
								<?php if ( $breadcrumbs ) woodmart_current_breadcrumbs( 'pages' ); ?>
							<?php endif ?>
						</header><!-- .entry-header -->
					</div>
				</div>
			<?php
			return;
		endif;


		// Heading for blog and archives
		if( $single_post_design != 'large_image' && is_singular( 'post' ) || woodmart_is_blog_archive() ):

			$title = ( ! empty( $page_for_posts ) ) ? get_the_title( $page_for_posts ) : esc_html__( 'Blog', 'woodmart' );

			if( is_tag() ) {
				$title = esc_html__( 'Tag Archives: ', 'woodmart')  . single_tag_title( '', false ) ;
			}

			if( is_category() ) {
				$title = '<span>' . single_cat_title( '', false ) . '</span>'; 
			}

			if( is_date() ) {
				if ( is_day() ) :
					$title = esc_html__( 'Daily Archives: ', 'woodmart') . get_the_date();
				elseif ( is_month() ) :
					$title = esc_html__( 'Monthly Archives: ', 'woodmart') . get_the_date( _x( 'F Y', 'monthly archives date format', 'woodmart' ) );
				elseif ( is_year() ) :
					$title = esc_html__( 'Yearly Archives: ', 'woodmart') . get_the_date( _x( 'Y', 'yearly archives date format', 'woodmart' ) );
				else :
					$title = esc_html__( 'Archives', 'woodmart' );
				endif;
			}

			if ( is_author() ) {
				/*
				 * Queue the first post, that way we know what author
				 * we're dealing with (if that is the case).
				 *
				 * We reset this later so we can run the loop
				 * properly with a call to rewind_posts().
				 */
				the_post();

				$title = esc_html__( 'Posts by ', 'woodmart' ) . '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>';

				/*
				 * Since we called the_post() above, we need to
				 * rewind the loop back to the beginning that way
				 * we can run the loop properly, in full.
				 */
				rewind_posts();
			}

			if( is_search() ) {
				$title = esc_html__( 'Search Results for: ', 'woodmart' ) . get_search_query();
			}

			?>
				<div class="page-title <?php echo esc_attr( $title_class ); ?> title-blog" style="<?php echo esc_attr( $style ); ?>">
					<div class="container">
						<header class="entry-header">
							<?php if( $page_title && is_single() ): ?>
								<h3 class="entry-title"><?php echo wp_kses( $title, woodmart_get_allowed_html() ); ?></h3>
							<?php elseif( $page_title ): ?>
								<h1 class="entry-title"><?php echo wp_kses( $title, woodmart_get_allowed_html() ); ?></h1>
							<?php endif; ?>
							<?php if ( $breadcrumbs && ! is_search() ) woodmart_current_breadcrumbs( 'pages' ); ?>
						</header><!-- .entry-header -->
					</div>
				</div>
			<?php
			return;
		endif;
		
		// Heading for portfolio
		if( is_singular( 'portfolio' ) || woodmart_is_portfolio_archive() ):
		
			if ( woodmart_get_opt( 'single_portfolio_title_in_page_title' ) ) {
				$title = get_the_title();
			} else {
				$title = get_the_title( $page_for_projects );
			}

			if( is_tax( 'project-cat' ) ) {
				$title = single_term_title( '', false );
			}

			?>
				<div class="page-title <?php echo esc_attr( $title_class ); ?> title-blog" style="<?php echo esc_attr( $style ); ?>">
					<div class="container">
						<header class="entry-header">
							<?php if( $page_title ): ?><h1 class="entry-title"><?php echo esc_html( $title ); ?></h1><?php endif; ?>
							<?php if ( $breadcrumbs ) woodmart_current_breadcrumbs( 'pages' ); ?>
						</header><!-- .entry-header -->
					</div>
				</div>
			<?php
			return;
		endif;

		// Page heading for shop page
		if( woodmart_is_shop_archive()
			&& ( $shop_categories || $shop_title )
		 ):

			if( is_product_category() ) {

		        $cat = $wp_query->get_queried_object();

				$cat_image = woodmart_get_category_page_title_image( $cat );

				if( $cat_image != '') {
					$style = "background-image: url(" . $cat_image . ")";
				}
			}

			if( is_product_category() || is_product_tag() ) {
				$title_class .= ' with-back-btn';
			}

			if( ! $shop_title ) {
				$title_class .= ' without-title';
			}

			?>
				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) && ! is_singular( "product" ) ) : ?>
					<div class="page-title <?php echo esc_attr( $title_class ); ?> title-shop" style="<?php echo esc_attr( $style ); ?>">
						<div class="container">
							<div class="nav-shop">

								<div class="shop-title-wrapper">
									<?php if ( is_product_category() || is_product_tag() ): ?>
										<?php woodmart_back_btn(); ?>
									<?php endif ?>

									<?php if ( $shop_title ): ?>
										<h1 class="entry-title"><?php woocommerce_page_title(); ?></h1>
									<?php endif ?>
								</div>
								
								<?php if( ! is_singular( "product" ) && $shop_categories ) woodmart_product_categories_nav(); ?>

							</div>
						</div>
					</div>
				<?php endif; ?>
			<?php
				$employee_notice = woodmart_child_employee_price_notice();
				if ( $employee_notice ) :
			?>
				<ul class="woocommerce-error employee-notice" role="alert">
					<li><?php echo $employee_notice; ?></li>
				</ul>
			<?php endif; ?>
			<?php return; ?>
		<?php
		endif;
}

/**
 * WooCommerce shop loop before.
 */
function woodmart_child_employee_price_notice() {
	if ( is_user_logged_in() && is_shop() ) {
		$user_id = get_current_user_id();
		$user_max_price = get_user_meta( $user_id, '_max_price', true );
		if ( ! empty( $user_max_price ) ) {
			$total = WC()->cart->total;
			$all_orders = woodmart_child_get_current_year_orders( $user_id );
			if ( ! empty( $all_orders ) ) {
				$all_order_price = array_column( $all_orders, 'price' );
				$total += array_sum( $all_order_price );
			}
			$total_amount = $user_max_price - $total;
			$total_amount = wc_price( $total_amount );
			$total_amount = str_replace( 'DKK', '', $total_amount );
			$total_amount = str_replace( '&nbsp;', '', $total_amount );
			return wp_sprintf( __( 'Du har %1$s,- kr tilbage at købe gaver for.', 'woodmart' ), $total_amount );
		}
	}
	return false;
}

/**
 * Get sub order by parent ID.
 *
 * @param int $parent_id Parent Order ID.
 * @return array.
 */
function woodmart_child_get_sub_orders( $parent_id = 0 ) {
	if ( $parent_id ) {
		$sub_orders = get_children(
			array(
				'post_type' => 'shop_order',
				'post_parent' => $parent_id,
				'fields' => 'ids',
			)
		);
		return $sub_orders;
	}
	return false;
}

/**
 * Update company ID.
 *
 * @param int $user_id User ID.
 * @return int|bool Defult false.
 */
function woodmart_child_update_company_id( $user_id ) {
	$company_id = get_user_meta( $user_id, '_company_id', true );
	if ( $company_id ) {
		$last_order = wc_get_customer_last_order( $company_id );
		$order_id = $last_order->get_id();
		$updated = update_user_meta( $user_id, '_last_company_order', $order_id );
		if ( $updated ) {
			return $order_id;
		} else {
			return false;
		}
	}
	return false;
}

/**
 * Get employee by company ID.
 *
 * @param int $company_id Company ID.
 * @return array
 */
function woodmart_get_employee_by_company( $company_id = 0, $args = array() ) {
	$customer_query = array(
		'role__in' => 'customer',
		'meta_key' => '_company_id',
		'meta_value' => $company_id,
		'fields' => 'ids',
	);
	if ( ! empty( $args ) ) {
		$customer_query = array_merge( $customer_query, $args );
	}
	$get_employee = get_users( $customer_query );
	return array_map( 'intval', $get_employee );
}

/**
 * Uplaod employee CSV File.
 *
 * @param array $file $_FILES Data.
 * @param array $user_id User ID.
 * @return int Default 0.
 */
function woodmart_child_upload_employee_csv_file( $file, $user_id = 0 ) {
	$filename = 'company-' . $user_id . '-' . basename( $file['employee_csv']['name'] );
	$file_data = file_get_contents( $file['employee_csv']['tmp_name'] );
	$upload_file = wp_upload_bits( $filename, null, $file_data );
	// If check file is uploaded OR not.
	if ( ! $upload_file['error'] ) {
		$wp_filetype = wp_check_filetype( $filename, null );
		$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_parent' => 0,
			'post_title' => preg_replace( '/\.[^.]+$/', '', $filename ),
			'post_content' => '',
			'post_status' => 'inherit',
		);
		$attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], 0 );
		if ( ! is_wp_error( $attachment_id ) ) {
			require_once( ABSPATH . 'wp-admin' . '/includes/image.php' );
			$attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
			wp_update_attachment_metadata( $attachment_id, $attachment_data );
			return $attachment_id;
		}
	}
	return 0;
}


/**
 * Convert a comma separated file into an associated array.
 * The first row should contain the array keys.
 *
 * @param string $filename File Name.
 * @param string $delimiter Delimiter.
 * @return array
 */
function woodmart_child_csv_to_array( $filename = '', $delimiter = ',' ) {
	// If check file exists AND readable OR Not.
	if ( ! file_exists( $filename ) || ! is_readable( $filename ) ) {
		return false;
	}
	$header = null;
	$data = array();
	if ( ( $handle = fopen( $filename, 'r' ) ) !== false ) {
		while ( ( $row = fgetcsv( $handle, 1000, $delimiter ) ) !== false ) {
			if( ! $header ) {
				$header = $row;
			} else {
				//$header = array_map( 'sanitize_title', $header );
				$header = array( 0, 1, 2 );
				$data[] = array_combine( $header, $row );
			}
		}
		fclose( $handle );
	}
	return $data;
}

/**
 * WooCommerce send company notification.
 *
 * @param string $to Email to.
 * @param string $password Password.
 * @param int $order_id Order ID.
 * @return bool
 */
function woodmart_child_send_company_notification( $to = '', $user_login ='', $password = '', $order = 0 ) {
	$headers = array(
		'Content-Type: text/html; charset=UTF-8',
		'From: ' . get_bloginfo( 'name' ) . ' <' . get_bloginfo( 'admin_email' ) . '>',
	);
	ob_start(); ?>
	<p><?php printf( esc_html__( 'Kære Gavekoordinater,', 'woodmart' ), esc_html( $user_login ) ); ?></p>

	<p><?php printf( esc_html__( 'Vi er glade for, at I vælger at købe årets julegaver lokalt.', 'woodmart' ) ); ?></p>
	<br>
	
	<p><?php printf( esc_html__( 'Hvad sker der nu?', 'woodmart' ) ); ?></p>
	<p><?php printf( esc_html__( 'Du skal ikke tænke på mere – vi gør arbejdet for dig!', 'woodmart' ) ); ?></p>
	<br>
	
	<p><?php printf( esc_html__( 'Dine medarbejdere vil i november modtage en e-mail med et unikt login. Når medarbejderen logger ind på hjemmesiden www.voresjulegave.dk, vil han/hun kunne vælge imellem de gaver/kategorier, som I har udvalgt til dem! Når medarbejderen har valgt en julegave, registrerer vi det helt automatisk', 'woodmart' ) ); ?></p><br>
	<p><?php printf( esc_html__( 'Du kan, som gavekoordinater, sikre dig at alle medarbejdere har valgt en julegave inden sidste frist.', 'woodmart' ) ); ?></p><br>
	<p><?php printf( esc_html__( 'Det gør du ved at logge ind på hjemmesiden: www.voresjulegave.dk med dit unikke login. Dit unikke login modtager du i November 2020.', 'woodmart' ) ); ?></p><br>
	<p><?php printf( esc_html__( 'Du kan, som gavekoordinater, sikre dig at alle medarbejdere har valgt en julegave inden sidste frist.', 'woodmart' ) ); ?></p><br>
	<p><?php printf( esc_html__( 'Har I problemer med det tekniske eller andre spørgsmål? ', 'woodmart' ) ); ?></p><br>
	<br>

	<p><?php printf( esc_html__( 'Kontakt:', 'woodmart' ) ); ?></p>
	<p><?php printf( esc_html__( 'Lea Frydendal', 'woodmart' ) ); ?></p>
	<p><?php printf( esc_html__( 'Tlf. 54552007', 'woodmart' ) ); ?></p>
	<p><?php printf( esc_html__( 'E-mail: lfh@provi.dk', 'woodmart' ) ); ?></p>
	<p><?php printf( esc_html__( 'Vi hjælper dig gerne godt på vej! ', 'woodmart' ) ); ?></p>

	<?php
	$body = ob_get_clean();
	wp_mail( $to, wp_sprintf( __( 'Din ordre er modtaget', 'woodmart' ), get_bloginfo( 'name' ) ), $body, $headers );
}

/**
 * Get current year order.
 *
 * @param int $customer_id Customer ID.
 * @return object|int.
 */
function woodmart_child_get_current_year_order( $customer_id ) {
	if ( $customer_id ) {
		$last_order = wc_get_customer_last_order( $customer_id );
		if ( $last_order ) {
			$order = wc_get_order( $last_order->get_id() );
			if ( $order ) {
				$customer   = get_user_by( 'ID', $order->get_user_id() );
				$item_count = $order->get_item_count() - $order->get_item_count_refunded();
				$last_order_year = date_i18n( 'Y', strtotime( $order->order_date ) );
				$current_year = date_i18n( 'Y', strtotime( 'now' ) );
				if ( $last_order_year == $current_year ) {
					return $order;
				}
			}
		}
	}
	return false;
}

/**
 * Get current year all orders.
 *
 * @param int $customer_id Customer ID.
 * @return mixed
 */
function woodmart_child_get_current_year_orders( $customer_id = 0 ) {
	$employee_order_query = array(
		'numberposts' => -1,
		'post_type'   => wc_get_order_types( 'view-orders' ),
		'post_status' => array_keys( wc_get_order_statuses() ),
		'meta_key'    => '_customer_user',
		'meta_value'  => $customer_id ? $customer_id : get_current_user_id(),
	);
	$customer_orders = get_posts( $employee_order_query );
	$order_data = [];
	if ( ! empty( $customer_orders ) ) {
		foreach ( $customer_orders as $customer_order ) {
			$order = new WC_Order( $customer_order->ID );
			$employee_csv_file = get_post_meta( $order->get_id(), 'employee_csv_file', true );
			if ( empty( $employee_csv_file ) ) {
				$last_order_year = date_i18n( 'Y', strtotime( $order->order_date ) );
				$current_year = date_i18n( 'Y', strtotime( 'now' ) );
				if ( $last_order_year == $current_year ) {
					$order_data[] = array(
						'id' => $order->get_id(),
						'price' => $order->get_total(),
					);
				}
			}
		}
	}
	return $order_data;
}

/**
 * Get all employee order amount by id.
 *
 * @param int $post_id Order ID.
 * @return Price
 */
function woodmart_child_get_all_employee_orders_total( $post_id = 0 ) {
	$order = new WC_Order( $post_id );
	$company = woodmart_get_employee_by_company( $order->get_user_id() );

	$employee_csv_file = get_post_meta( $order->get_id(), 'employee_csv_file', true );
	$employee_csv_file = get_attached_file( $employee_csv_file );
	$employee_csv_file = woodmart_child_csv_to_array( $employee_csv_file );

	//$price = isset( $employee[2] ) && ! empty( $employee[2] ) ? $employee[2] : '';

	$company[] = $order->get_user_id();
	$company_order_total = [];
	/*if ( ! empty( $company ) ) {
		foreach ( $company as $cid ) {
			$employee_price = get_user_meta( $cid, '_max_price', true );
			if ( $employee_price ) {
				$company_order_total[] = $employee_price;
			}
		}
	}*/
	if ( ! empty( $employee_csv_file ) ) {
		foreach ( $employee_csv_file as $cid ) {
			$price = isset( $cid[2] ) && ! empty( $cid[2] ) ? $cid[2] : 0;
			if ( $price ) {
				$company_order_total[] = $price;
			}
		}
	}

	if ( ! empty( $company_order_total ) ) {
		$company_order_total = array_sum( $company_order_total );
		return $company_order_total;
	} else {
		return 0;
	}
}

/**
 * Send message after user create succesfully from admin.
 */
add_filter( 'wp_new_user_notification_email', 'custom_wp_new_user_notification_email', 10, 3 );
function custom_wp_new_user_notification_email( $wp_new_user_notification_email, $user, $blogname ) {
    // Generate a new key
    $key = get_password_reset_key( $user );
    if( $user->roles[0] == 'customer' ) { 
	    $message = __('<strong>Kære'.$user->display_name.'</strong></p>,') . "<br/><br/>";
	    $message .= sprintf(__('Du har fortjent en julegave!') ). "<br/><br/>";
	    $message .= __("Derfor har din arbejdsgiver gjort det muligt for dig at vælge lige det du ønsker dig i år – og det gør du på hjemmesiden: www.voresjulegave.dk ") . "<br/><br/>";
	    $message .= __("Julegaverne er lokale – og unikt udvalgt til dig. Derfor kan du nu logge på din nye konto for at vælge en julegave på:https://www.voresjulegave.dk/min/konto/") . "<br/><br/>";
	    
	    $message .= sprintf(__('Brugernavn: %s'), $user->user_login) . "<br/>";
	    $message .= sprintf(__('Adgangskode: ') ) . "<br/>";
	    $message .= network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user->user_login), 'login') . "<br/><br/>";
	    
	    $message .= __("Når du logger ind på Vores Julegave, har du mulighed for at vælge præcis den julegave, som du har ønsket dig i år. Det gør du ved at gennemse de udvalgte kategorier – og klikke på Vælg Julegave, når du har besluttet dig. Bekræft herefter dit valg og log ud igen – nemmere bliver det ikke.") . "<br/><br/>";
	    $message .= __("Din julegave skal være valgt senest d. xx. september 2020") . "<br/><br/>";
	    $message .= __("Tekniske problemer? Tag kontakt til virksomhedens gavekoordinater, som vil kontakte serviceteamet. ") . "<br/><br/>";
	    $message .= __("Venligst") . "<br/><br/>";
	    $message .= __("Vores Julegave") . "<br/><br/>";
	}

	if( $user->roles[0] == 'company_coordinator' ) { 
	    $message = __('<strong>Kære Gavekoordinater</strong>,') . "<br/>";
	    $message .= sprintf(__('Du modtager nu dit personlige login til www.voresjulegave.dk') ). "<br/><br/>";
	    
	    $message .= sprintf(__('Brugernavn: %s'), $user->user_login) . "<br/>";
	    $message .= sprintf(__('Adgangskode: ') ) . "<br/>";
	    $message .= network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user->user_login), 'login') . "<br/><br/>";
	    $message .= __("Med dette login kan du tilgå <a href='https://www.voresjulegave.dk/min-konto/' target='_blank'>min konto</a> på www.voresjulegave.dk") . "<br/><br/>";
	    
	    $message .= __("Her har du mulighed for at klikke på Medarbejder Ordre, som giver dig et fuldt overblik over hvilke medarbejdere, som har bestilt – og i så fald hvad de har bestilt.") . "<br/><br/>";
	    $message .= __("Det nederste beløb under listevisningen: <a href='https://www.voresjulegave.dk/min-konto/orders/' target='_blank'>Ordre</a> vidner om hvorvidt I har et restbeløb tilgodehavende.") . "<br/><br/>";
	    $message .= __("<strong>Husk din medarbejdere på at få valgt deres julegave inden d. 23. oktober 2020</strong>") . "<br/><br/>";
	    $message .= __("Har I problemer med det tekniske eller andre spørgsmål?") . "<br/><br/>";
	    $message .= __("<strong>Kontakt:</strong>") . "<br/><br/>";
	    $message .= __("Lea Frydendal") . "<br/><br/>";
	    $message .= __("Tlf. 54552007") . "<br/><br/>";
	    $message .= __("E-mail: <a href='mailto:lfh@provi.dk'>lfh@provi.dk</a>") . "<br/><br/>";
	    $message .= __("Vi hjælper dig gerne godt på vej!") . "<br/><br/>";
	}

    $wp_new_user_notification_email['headers'] = array('Content-Type: text/html; charset=UTF-8');
    $wp_new_user_notification_email['message'] = $message;
    return $wp_new_user_notification_email;
}

/*
** Create admin menu for gift selection page 
*/
add_menu_page(
        __( 'Vælg standard gave', 'vinogvelsmag' ),
        'Vælg standard gave',
        'manage_options',
        'gift_selection',
        'vino_menu_card_callback','','22'
      );

function vino_menu_card_callback(){
  wp_safe_redirect(admin_url('post.php?post=394&action=edit'));
}

/*
** Hide Gift Selection page from page list admin side
*/
add_filter( 'parse_query', 'exclude_pages_from_admin' );
function exclude_pages_from_admin($query) {
    global $pagenow,$post_type;
    if (is_admin() && $pagenow=='edit.php' && $post_type =='page') {
        $query->query_vars['post__not_in'] = array('394');
    }
}

/**
 * Disable Gutenberg for Gift Selection page
 *
 */
function ea_disable_gutenberg( $can_edit, $post_type ) {

  if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
    return $can_edit;

  if( $_GET['post'] == '394' ) {
    $can_edit = false;
    hide_composer_switch_btns();
  }
  return $can_edit;

}
add_filter( 'gutenberg_can_edit_post_type', 'ea_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2 );

/**
 * Disable Classic Editor for Gift Selection page
 *
 */
function ea_disable_classic_editor() {

  $screen = get_current_screen();
  if( 'page' !== $screen->id || ! isset( $_GET['post']) )
    return;

  if( $_GET['post'] == '394' ) {
    remove_post_type_support( 'page', 'editor' );
    hide_composer_switch_btns();
  }

}
add_action( 'admin_head', 'ea_disable_classic_editor' );

function hide_composer_switch_btns(){
  echo "<style>
          .composer-switch,
          .wp-heading-inline,
          .page-title-action{
            display: none !important;
          }
          #delete-action,
          #edit-slug-box{
            display: none;
          }
        </style>";
}

add_action( 'admin_head-post.php', 'vino_highlight_menu_item' );
function vino_highlight_menu_item()
{
    global $current_screen;

    // Not our post type, exit earlier
    if( 'page' != $current_screen->post_type )
        return;

    if( '394' == $_GET['post'] )
    {       
        ?>
        <script type="text/javascript">
            jQuery(document).ready( function($) 
            {
                var reference = jQuery('#toplevel_page_gift_selection');

                // add highlighting to our custom submenu
                reference.addClass('current');

                //remove higlighting from the default menu
                jQuery('#menu-pages').removeClass('wp-has-current-submenu').addClass('wp-not-current-submenu');         
            });     
        </script>
        <?php
    }
}

/*add price with product title in acf relationship field*/
add_filter('acf/fields/relationship/result/name=select_product', 'my_acf_fields_relationship_result', 10, 4);
function my_acf_fields_relationship_result( $text, $post, $field, $post_id ) {
	$product = new WC_Product( $post->ID );
    $text = $text . ' ' . wc_price( $product->get_price() );
    return $text;
}

/**
 * wp crone for create customer order 
 */
if ( ! wp_next_scheduled( 'create_customer_orders' ) ) {
	wp_schedule_event(strtotime('00:30:00'), 'daily', 'create_customer_orders');
}

function create_customer_order_data(){						   
	// Get current date
	$current_date = date("d/m/Y");
	// Get order under processing
	$args = array(
	    'status' => 'wc-processing',
	    'return' => 'ids',
	);
	$orders = wc_get_orders( $args );

	$custmr_id = array();
	$usr_price_data = array();

	foreach ($orders as $key => $order_id) {
		$final_date = get_field( 'final_order_date', $order_id );
		
		// Check final date equal to current date
		if( $current_date == $final_date ){

			$order = wc_get_order( $order_id );
			// Get the Customer ID (User ID)
			$customer_id = $order->get_customer_id();
			// Get the WP_User Object instance
			$user = $order->get_user();
			// Get the WP_User roles and capabilities
			$user_roles = $user->roles;
			// Check user role is company coordinator nad their customer user list
			if( $user_roles[0] == 'company_coordinator' ){
				$customer_query = array(
					'role__in' => 'customer',
					'meta_key' => '_company_id',
					'meta_value' => $customer_id,
					'fields' => 'ids',
				);
				$get_employee_id = get_users( $customer_query );

				foreach ($get_employee_id as $key => $employee_id) {
					// Check customer user have any order	
					$usr_orders = get_posts( array(
				        'numberposts' => -1,
				        'meta_key'    => '_customer_user',
				        'meta_value'  => $employee_id,
				        'post_type'   => wc_get_order_types( 'view-orders' ),
				        'post_status' => array_keys( wc_get_order_statuses() )
				    ) );

				    $order_date = $usr_orders[0]->post_date;
					$order_time = strtotime($order_date);
					$order_year = date("Y", $order_time);

				    if ( ( $order_year != date("Y") ) ){
				    	$custmr_id[] = $employee_id;
				    	$usr_price_data[] = get_user_meta( $employee_id, '_max_price', true );				
				    }
				}
			}
		}
	}

	//Get gift selection data
	$gift_select = get_field_object('gift_selection', 394); 	

	if( $gift_select['value'] ): 
		$price_array = array_values(array_column($gift_select['value'], 'price'));
		$product_array = array_values(array_column($gift_select['value'], 'select_product'));

		$prd_price_arr = array();
		$prd_price = array();

		foreach ($product_array as $prod_value) {
			foreach ($prod_value as $key => $prod_val) {
				//get product id
				$product = wc_get_product( $prod_val );
				//get product price
				if( $product->is_on_sale() ) {
					$prd_price[] = round( $product->get_sale_price(), 0 );
					$prd_price_arr[$prod_val] = round( $product->get_sale_price(), 0 );
				}else{
					$prd_price[] = round( $product->get_regular_price(), 0 );
					$prd_price_arr[$prod_val] = round( $product->get_regular_price(), 0 );
				}
			}
		}
	endif;				

	//get selected product id from gift select page
	$final_prd_id = array();

	foreach ($usr_price_data as $key => $num_val) {
		//get nearest price value
		foreach ($price_array as $i) {
		    $nearest_price[$i] = abs($i - $num_val);
		}
		asort($nearest_price);
		$price_val = key($nearest_price);
		//get product id from nearest product price
		foreach ($prd_price_arr as $key => $price_arr_val) {
			$final_data[$key] = abs($price_arr_val - $price_val);
		}
		asort($final_data);
		$final_prd_id[] = key($final_data);
	}
	
	//Process order for customer users
	$cutm_id = $custmr_id;
	$pro_id = $final_prd_id;
	$usr_data = array_combine($cutm_id, $pro_id);

	foreach ($usr_data as $key => $value) {
		$usr_id = $key;
		$prd_id = $value;
		$product = wc_get_product( $prd_id );

		if( $product->is_on_sale() ) {
			$price_data = $product->get_sale_price();
		}else{
			$price_data = $product->get_regular_price();						
		}

		global $woocommerce;

		$user = get_user_by('id', $usr_id);						    
	    $first_name = $user->first_name;
	    $last_name = $user->last_name;
	    $email = $user->user_email;

	    $address = array(
	        'first_name' => $first_name,
	        'last_name'  => $last_name,
	        'email'      => $email,
	    );
	    // Now we create the order
	    $default_args = array(
	        'customer_id' =>  $usr_id,
	        'status' => 'completed'
	    );
	    $product = wc_get_product( $prd_id );
	    $product->set_price( strval( $price_data ) );
	    
	    $order = wc_create_order( $default_args );

	    $order->add_product( $product , 1 ); // Use the product IDs to add
	    // Set addresses
	    $order->set_address( $address, 'billing' );
	    $order->set_address( $address, 'shipping' );

	    update_post_meta($order->id, '_customer_user', $usr_id);

	    $note = __("Vælg standard gave");
		// Add the note
		$order->add_order_note( $note );

	    $order->calculate_totals(); 
	}
}
add_action( 'create_customer_orders', 'create_customer_order_data', 10 );