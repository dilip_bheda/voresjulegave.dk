<?php
/**
 * Enqueue script and styles for child theme
 */
function woodmart_child_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'woodmart-style' ), woodmart_get_theme_info( 'Version' ) );
}
add_action( 'wp_enqueue_scripts', 'woodmart_child_enqueue_styles', 10010 );

// Customize WooCommerce.
require_once 'inc/woo-gift-voucher.php';

function custom_my_account_menu_items( $items ) {
    unset($items['downloads']);
    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'custom_my_account_menu_items' );

// To change add to cart text on single product page
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' ); 
function woocommerce_custom_single_add_to_cart_text() {
    return __( 'Vælg gave', 'woocommerce' ); 
}

// To change add to cart text on product archives(Collection) page
add_filter( 'woocommerce_product_add_to_cart_text', 'woocommerce_custom_product_add_to_cart_text' );  
function woocommerce_custom_product_add_to_cart_text() {
    return __( 'Vælg gave', 'woocommerce' );
}

// Easily do user specific CSS
function add_css_to_guest_user() {
    $guest_user = wp_get_current_user();
 
    if($guest_user && isset($guest_user->user_login) && 'guest' == $guest_user->user_login) {
        wp_enqueue_style( 'guest_admin_css', get_stylesheet_directory_uri() . '/css/wpadmin-guest.css' );
    }
}
add_action('admin_print_styles', 'add_css_to_guest_user' );

add_filter( 'woodmart_use_custom_price_widget', '__return_false', 10 );
add_filter( 'woodmart_use_custom_order_widget', '__return_false', 10 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

/**
 * Tilfældig rækkefølge
 */

add_filter( 'woocommerce_catalog_orderby', 'woo_random_sorting_option' );
 
function woo_random_sorting_option( $options ){
 
	$options['rand'] = 'Sorter tilfældig rækkefølge';
	return $options;
 
}

add_filter('woocommerce_default_catalog_orderby', 'misha_default_catalog_orderby');
 
function misha_default_catalog_orderby( $sort_by ) {
	return 'rand';
}