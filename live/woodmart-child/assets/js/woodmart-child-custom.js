jQuery( document ).ready( function( $ ) {
	// Ajax loader.
	var ajaxLoader = function() {
		var spinner = $( '.wood-form-submit' ).find( '.spinner' );
		if ( spinner.hasClass( 'is-active' ) ) {
			spinner.removeClass( 'is-active' );
		} else {
			spinner.addClass( 'is-active' );
		}
	};

	/**
	 * Submit company form.
	 */
	var submitCompanyForm = function( is_csv = false ) {
		// Check is CSV file.
		if ( is_csv ) {
			$( 'input[name="action"]' ).val( 'woodmart_upload_company_csv' );
		} else {
			$( 'input[name="action"]' ).val( 'woodmart_create_company' );
		}
		var formData = new FormData( $( '#bestil_julegaver' )[0] );
		$.ajax( {
			url: WOOD_CHILD.ajax_url,
			type: 'post',
			data: formData,
			dataType: 'json',
			contentType: false,
			processData: false
		} )
		.done( function( response ) {
			ajaxLoader();
			if ( response.message != '' ) {
				if ( response.result ) {
					if ( ! is_csv ) {
						$( '#bestil_julegaver' )[0].reset();
					}
				}
				if ( response.message && response.message != '' ) {
					$( '#employee_csv' ).next( 'span' ).hide();
					$( '.wood-resonse-notice' ).html( response.message );
					$( '.wood-resonse-notice' ).show();
					if ( response.order_for_pay && response.order_for_pay != '' ) {
						window.location = response.order_for_pay;
					}
				}
				if ( response.total_amount && response.total_amount != '' ) {
					$( '.show-total-amount' ).html( response.total_amount );
					$( '.demo-csv-file' ).addClass( 'price-added' );
					$( '.show-total-amount' ).show();
				}
			}
		} )
		.fail( function() {
			ajaxLoader();
			$( '.demo-csv-file' ).removeClass( 'price-added' );
			$( '.show-total-amount' ).hide();
			//alert( 'Noget gik galt. Prøv igen' );
		} );
	};

	// Form validate.
	if ( $( '#bestil_julegaver' ).length > 0 ) {
		$( '#bestil_julegaver' ).validate( {
			errorClass: 'error',
			errorElement: 'span',
			rules: {
				company: {
					required: true,
				},
				address: {
					required: true,
				},
				contact_person: {
					required: true,
				},
				telephone_number: {
					required: true,
					digits: true,
				},
				email: {
					required: true,
					email: true,
				},
				employee_csv: {
					required: true,
					extension: "xls|csv",
				},
				agree: {
					required: true,
				},
			},
			messages: {
				company: {
					required: "Dette felt er påkrævet.",
				},
				agree: {
					required: "Dette felt er påkrævet.",
				},
				address: {
					required: "Dette felt er påkrævet.",
				},
				contact_person: {
					required: "Dette felt er påkrævet.",
				},
				telephone_number: {
					required: "Dette felt er påkrævet.",
					digits: "Indtast kun numre.",
				},
				email: {
					required: "Dette felt er påkrævet.",
					email: "Indtast venligst en gyldig e-mailadresse.",
				},
				ean_number: {
					required: "Dette felt er påkrævet.",
				},
				employee_csv: {
					required: "Dette felt er påkrævet.",
					extension: "Upload kun CSV-fil.",
				}
			},
			errorPlacement: function( error, element ) {
				if ( $( element ).hasClass( 'term-and-condition' ) ) {
					error.insertAfter( $( element ).parent() );
				} else {
					error.insertAfter( element );
				}
			},
			submitHandler: function( form ) {
				ajaxLoader();
				submitCompanyForm();
			}
		} );
	}
	// Display filename.
	$( document ).on( 'change', '#employee_csv', function(e){
		var fileName = e.target.files[0].name;
		$( this ).next( 'span' ).html( fileName ).show();
		ajaxLoader();
		$( '.demo-csv-file' ).removeClass( 'price-added' );
		$( '.show-total-amount' ).hide();
		submitCompanyForm( true );
	} );
	// Update cart total.
	$( document.body ).on( 'updated_cart_totals removed_from_cart', function() {
		var cartAmount = $( 'ul.woocommerce-error' ).find( '.woocommerce-Price-amount.amount' ).html();
		$( '.employee-wallet' ).find( '.woodmart-cart-totals' ).html( cartAmount );
	} );
} );