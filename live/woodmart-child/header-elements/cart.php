<?php
$extra_class = $custom_icon = $custom_icon_width = $custom_icon_height = '';
$icon_type = $params['icon_type'];
$cart_position = $params['position'];

$extra_class .= ' woodmart-cart-design-'. $params['style']; 

if ( $icon_type == 'bag' ) {
	$extra_class .= ' woodmart-cart-alt';
}

if ( $icon_type == 'custom' ) {
	$extra_class .= ' wd-tools-custom-icon';
}

if ( $cart_position == 'side' ) {
	$extra_class .= ' cart-widget-opener';
}


if ( ! woodmart_woocommerce_installed() || $params['style'] == 'disable' || ( ! is_user_logged_in() && woodmart_get_opt( 'login_prices' ) ) ) return; ?>

<div class="woodmart-shopping-cart wd-tools-element<?php echo esc_attr( $extra_class ); ?>" title="<?php echo esc_attr__( 'Shopping cart', 'woodmart' ); ?>">
	<a href="<?php echo esc_url( wc_get_cart_url() ); ?>">
		<span class="woodmart-cart-icon wd-tools-icon">
			<?php
				if ( $icon_type == 'custom' ) {
					echo whb_get_custom_icon( $params['custom_icon'] );
				}
			?>
			
			<?php if ( '2' == $params['style'] || '5' == $params['style'] ) : ?>
				<?php woodmart_cart_count(); ?>
			<?php endif; ?>
		</span>
		<span class="woodmart-cart-totals wd-tools-text">
			<?php if ( '2' != $params['style'] && '5' != $params['style'] ) : ?>
				<?php woodmart_cart_count(); ?>
			<?php endif; ?>

			<span class="subtotal-divider">/</span>
			<?php woodmart_cart_subtotal(); ?>
		</span>
	</a>
	<?php if ( $cart_position != 'side' && $cart_position != 'without' ): ?>
		<div class="dropdown-cart">
			<?php the_widget( 'WC_Widget_Cart', 'title=' ); ?>
		</div>
	<?php endif; ?>
</div>
<?php if ( is_user_logged_in() && ( current_user_can( 'customer' ) || current_user_can( 'company_coordinator' ) ) ) : ?>
<div class="woodmart-shopping-cart employee-wallet wd-tools-element<?php echo esc_attr( $extra_class ); ?>" title="<?php echo esc_attr__( 'Shopping cart', 'woodmart' ); ?>">
	<a href="javascript:;">
		<i class="fas fa-wallet"></i>
		<span class="woodmart-cart-totals wd-tools-text">
			<?php
			$user_id = get_current_user_id();
			$user_max_price = get_user_meta( $user_id, '_max_price', true );
			if ( ! empty( $user_max_price ) ) {
				$total = WC()->cart->total;
				$all_orders = woodmart_child_get_current_year_orders( $user_id );
				if ( ! empty( $all_orders ) ) {
					$all_order_price = array_column( $all_orders, 'price' );
					$total += array_sum( $all_order_price );
				}
				$total_amount = $user_max_price - $total;
				echo wc_price( $total_amount );
			}
		?>
		</span>
	</a>
</div>
<?php endif;