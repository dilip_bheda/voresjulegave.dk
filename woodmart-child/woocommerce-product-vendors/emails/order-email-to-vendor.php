<?php
/**
 * Order email to vendor.
 *
 * @version 2.1.0
 * @since 2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
	$order_date = $order->get_date_created();
	$billing_first_name = $order->get_billing_first_name();
	$billing_last_name = $order->get_billing_last_name();
} else {
	$order_date = $order->order_date;
	$billing_first_name = $order->billing_first_name;
	$billing_last_name = $order->billing_last_name;
}
?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php
	$customer_id = $order->get_user_id();
	$company_id = get_user_meta( $customer_id, '_company_id', true );
	$company_id = $company_id ? (int) $company_id : 0;
	$company_name = '';
	if ( $company_id ) {
		$company_info = get_user_by( 'ID', $company_id );
		if ( $company_info ) {
			$company_name = get_user_meta( $company_info->ID, 'billing_company', true );
		}
	}
?>
<p><?php printf( esc_html__( 'Du har modtaget en ordre fra medarbejderen %s fra firmaet %s
Når du pakker ordren, er det vigtigt at du notere medarbejder navnet og firmaet uden på pakken.', 'woocommerce-product-vendors' ), esc_html( $billing_first_name ) . ' ' . esc_html( $billing_last_name ), $company_name ); ?></p>

<p><?php printf( esc_html__( 'Ordren består af følgende:', 'woocommerce-product-vendors' ) ); ?></p>

<h2><?php printf( esc_html__( 'Order #%s', 'woocommerce-product-vendors' ), $order->get_order_number() ); ?> (<?php printf( '<time datetime="%s">%s</time>', date_i18n( 'c', strtotime( $order_date ) ), date_i18n( wc_date_format(), strtotime( $order_date ) ) ); ?>)</h2>

<?php $email->render_order_details_table( $order, $sent_to_admin, $plain_text, $email, $this_vendor ); ?>

<?php do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email ); ?>

<?php do_action( 'wc_product_vendors_email_order_meta', $order, $sent_to_admin, $plain_text, $email ); ?>

<?php do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email ); ?>

<?php do_action( 'woocommerce_email_footer', $email ); ?>
