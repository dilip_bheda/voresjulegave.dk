<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 */

get_header(); ?>


<?php 
	
	// Get content width and sidebar position
	$content_class = woodmart_get_content_class();

?>

<div class="site-content <?php echo esc_attr( $content_class ); ?>" role="main">

		<?php /* The loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<div class="entry-content vendor-content">
						<?php the_content(); ?>
						<?php
							$product_vendors = get_terms(
								array(
									'taxonomy' => 'wcpv_product_vendors',
									'hide_empty' => true,
								)
							);
							$product_vendors = woodmart_child_array_3d( $product_vendors );
							if ( ! empty( $product_vendors) ) :
							foreach( $product_vendors as $product_vendor ) :
						?>
						<div class="vc_row wpb_row vc_row-fluid vc_custom_1602507857693">
								<?php
								foreach( $product_vendor as $vendor ) :
									$vendor_data = WC_Product_Vendors_Utils::get_vendor_data_by_id( $vendor->term_id );
									$user_data = get_user_by( 'email', $vendor_data['email'] );
									$website = '';
									$address = '';
									$domain = '';
									if ( $user_data ) {
										$website = $user_data->user_url;
										if ( ! empty( $website ) ) {
											$domain = parse_url( $website );
											$domain = isset( $domain['host'] ) ? $domain['host'] : '';
											if ( strpos( $domain, 'www.' ) === false ) {
												$domain = 'www.' . $domain;
											}
										}
										$address = get_user_meta( $user_data->ID, 'billing_address_1', true );
									}
								?>
								<div class="wpb_column vc_column_container vc_col-sm-3">
									<div class="vc_column-inner vc_custom_1602507554305">
										<div class="wpb_wrapper">
											<div class="wpb_single_image wpb_content_element vc_align_center vc_custom_1602508116537">
												<figure class="wpb_wrapper vc_figure">
													<div class="vc_single_image-wrapper vc_box_border_grey">
														<?php if ( $vendor_data['logo'] ) : ?>
															<?php echo wp_get_attachment_image( $vendor_data['logo'], 'medium', '', array( 'class' => 'vendor-image-page' ) ); ?>
														<?php else : ?>
															<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/default.jpg" class="attachment-medium size-medium vendor-image" loading="lazy">
														<?php endif; ?>
													</div>
												</figure>
											</div>

											<div class="wpb_text_column wpb_content_element">
												<div class="wpb_wrapper vendor-info-page">
													<p style="text-align: center;">
														<!-- <?php // echo $vendor_data['name']; ?><br /> -->
														<?php if ( ! empty( $address ) ) : ?>
															<?php echo str_replace( ',', '<br>', $address ); ?><br>
														<?php endif; ?>
														<a href="<?php echo esc_url( $website ); ?>" target="_blank"><?php echo $domain; ?></a> 
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
						<?php endforeach; endif; ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'woodmart' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div>

					<?php woodmart_entry_meta(); ?>

				</article><!-- #post -->

				<?php 
					// If comments are open or we have at least one comment, load up the comment template.
					if ( woodmart_get_opt('page_comments') && (comments_open() || get_comments_number()) ) :
						comments_template();
					endif;
				 ?>

		<?php endwhile; ?>

</div><!-- .site-content -->

<style>
	.vc_custom_1602507857693 {
		padding-top: 75px !important;
		padding-bottom: 0px !important;
	}
	.vc_custom_1602507857693 {
		padding-top: 75px !important;
		padding-bottom: 0px !important;
	}
	.vc_custom_1602507857693 {
		padding-top: 75px !important;
		padding-bottom: 0px !important;
	}
</style>
<?php get_sidebar(); ?>

<?php get_footer(); ?>
