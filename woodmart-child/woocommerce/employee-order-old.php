<?php
$orders_columns = apply_filters(
	'woocommerce_account_employee_orders_columns',
	array(
		'order-number'  => __( 'Order', 'woodmart' ),
		'employee-name'  => __( 'Name', 'woodmart' ),
		'employee-email'  => __( 'Email', 'woodmart' ),
		'order-date'    => __( 'Date', 'woodmart' ),
		'order-status'  => __( 'Status', 'woodmart' ),
		'order-total'   => __( 'Total', 'woodmart' ),
		'order-actions' => __( 'Actions', 'woodmart' ),
	)
);
$employee_ids = array();
if ( is_user_logged_in() && current_user_can( 'company_coordinator' ) ) {
	$employee_ids = woodmart_get_employee_by_company( get_current_user_id() );
}

$employee_order_query = array(
	'numberposts' => -1,
	'post_type'   => wc_get_order_types( 'view-orders' ),
	'post_status' => array_keys( wc_get_order_statuses() ),
);
if ( ! empty( $employee_ids ) ) {
	foreach ( $employee_ids as $employee_id ) {
		$employee_order_query['meta_query'][] = array(
			'key' => '_customer_user',
			'value' => $employee_id,
		);
	}
	$employee_order_query['meta_query']['relation'] = 'OR';
}

$customer_orders = get_posts( apply_filters( 'woocommerce_my_account_employee_orders_query', $employee_order_query ) );

if ( $customer_orders ) : ?>

	<table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
		<thead>
			<tr>
				<?php foreach ( $orders_columns as $column_id => $column_name ) : ?>
					<th class="woocommerce-orders-table__header woocommerce-orders-table__header-<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
				<?php endforeach; ?>
			</tr>
		</thead>

		<tbody>
			<?php
			$order_complated_user = [];
			$order_total = [];
			foreach ( $customer_orders as $customer_order ) {
				$order      = wc_get_order( $customer_order ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
				$customer   = get_user_by( 'ID', $order->get_user_id() );
				$item_count = $order->get_item_count() - $order->get_item_count_refunded();
				$last_order_year = date_i18n( 'Y', strtotime( $order->order_date ) );
				$current_year = date_i18n( 'Y', strtotime( 'now' ) );
				if ( $last_order_year == $current_year ) {
					$order_complated_user[] = $customer->ID;
				}
				$order_total[] = $order->get_total();
				?>
				<tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-<?php echo esc_attr( $order->get_status() ); ?> order">
					<?php foreach ( $orders_columns as $column_id => $column_name ) : ?>
						<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
							<?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
								<?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

							<?php elseif ( 'order-number' === $column_id ) : ?>
								<a href="<?php echo esc_url( $order->get_view_order_url() ); ?>">
									<?php echo esc_html( _x( '#', 'hash before order number', 'woodmart' ) . $order->get_order_number() ); ?>
								</a>

							<?php elseif ( 'order-date' === $column_id ) : ?>
								<time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></time>

							<?php elseif ( 'order-status' === $column_id ) : ?>
								<?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>

							<?php elseif ( 'employee-name' === $column_id ) : ?>
								<?php echo esc_html( $customer->display_name ); ?>

							<?php elseif ( 'employee-email' === $column_id ) : ?>
								<?php echo esc_html( $customer->user_email ); ?>

							<?php elseif ( 'order-total' === $column_id ) : ?>
								<?php
								/* translators: 1: formatted order total 2: total order items */
								echo wp_kses_post( sprintf( _n( '%1$s for %2$s item', '%1$s for %2$s items', $item_count, 'woodmart' ), $order->get_formatted_order_total(), $item_count ) );
								?>

							<?php elseif ( 'order-actions' === $column_id ) : ?>
								<?php
								$actions = wc_get_account_orders_actions( $order );

								if ( ! empty( $actions ) ) {
									foreach ( $actions as $key => $action ) { // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
										echo '<a href="' . esc_url( $action['url'] ) . '" class="woocommerce-button button ' . sanitize_html_class( $key ) . '">' . esc_html( $action['name'] ) . '</a>';
									}
								}
								?>
							<?php endif; ?>
						</td>
					<?php endforeach; ?>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>
	<?php else : ?>
	<div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
		<a class="woocommerce-Button button" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			<?php esc_html_e( 'Browse products', 'woodmart' ); ?>
		</a>
		<?php esc_html_e( 'No order has been made yet.', 'woodmart' ); ?>
	</div>
	<?php endif; ?>
	<?php
	$exclude_user_query = array( 'exclude' => $order_complated_user );
	$exclude_user_query = array_filter( $exclude_user_query );
	$get_customers = woodmart_get_employee_by_company( get_current_user_id(), $exclude_user_query );
	if ( ! empty( $get_customers ) ) :
	?>
	<div class="pending-orders-list">
		<h4><?php esc_html_e( 'Samlet ordreoversigt', 'woodmart' ); ?></h4>
	</div>
	<div class="order-totals">
		<div class="all-order">
			<h3><?php esc_html_e( 'ordrer i alt', 'woodmart' ); ?></h3>
			<?php
				$order_total = ! empty( $order_total ) ? array_sum( $order_total ) : 0;
				echo wc_price( $order_total );
			?>
		</div>
		<div class="employee-total">
			<h3><?php esc_html_e( 'Samlet medarbejderbeløb', 'woodmart' ); ?></h3>
			<?php
				$employee_total_amount = [];
				foreach ( $get_customers as $c ) {
					$employee_total_amount[] = get_user_meta( $c, '_max_price', true );
				}
				$employee_amount_total = ! empty( $employee_total_amount ) ? array_sum( $employee_total_amount ) : 0;
				echo wc_price( $employee_amount_total );
			?>
		</div>
		<div class="all-totals">
			<h3><?php esc_html_e( 'tilgængeligt beløb', 'woodmart' ); ?></h3>
			<?php
				$available_amount = $employee_amount_total - $order_total;
				echo wc_price( $available_amount );
			?>
		</div>
	</div>
	<hr style="border-bottom: 2px solid #EFEFEF; width: 100%; border-top: none;">
	<div class="pending-orders-list">
		<h4><?php esc_html_e( 'Pending Order', 'woodmart' ); ?></h4>
	</div>
	<table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
		<thead>
			<tr>
				<th class="woocommerce-orders-table__header"><span class="nobr"><?php _e( 'Name', 'woodmart' ); ?></span></th>
				<th class="woocommerce-orders-table__header"><span class="nobr"><?php _e( 'Email', 'woodmart' ); ?></span></th>
				<th class="woocommerce-orders-table__header"><span class="nobr"><?php _e( 'Price', 'woodmart' ); ?></span></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ( $get_customers as $customer_id ) :
				$customer_info = get_user_by( 'ID', $customer_id );
				$first_name = $customer_info->first_name;
				$last_name = $customer_info->last_name;
				$name = $customer_info->display_name;
				if ( ! empty( $first_name ) && ! empty( $last_name ) ) {
					$name = $first_name . ' ' . $last_name;
				}
				// Price.
				$max_price = get_user_meta( $customer_id, '_max_price', true );
				if ( $max_price ) {
					$max_price = wc_price( $max_price );
				} else {
					$max_price = 0;
				}
			?>
				<tr class="woocommerce-orders-table__row">
					<td class="woocommerce-orders-table__cell"><?php echo $name; ?></td>
					<td class="woocommerce-orders-table__cell"><?php echo $customer_info->user_email; ?></td>
					<td class="woocommerce-orders-table__cell"><?php echo $max_price; ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?php endif; ?>
