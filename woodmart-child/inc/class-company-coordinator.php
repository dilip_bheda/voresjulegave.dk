<?php
/**
 * Company coordinator list table.
 *
 * @package WordPress
 * @subpackage Woodmart
 */

// If check class exist OR not.
if ( ! class_exists( 'Company_Coordinator' ) ) {

	/**
	 * Declare `Company_Coordinator` extend to `WP_List_Table`.
	 */
	class Company_Coordinator extends WP_List_Table {

		/**
		 * Calling Class constructor
		 */
		public function __construct() {
			// Call parent class.
			parent::__construct(
				[
					'singular' => __( 'Virksomhedskoordinator', 'woodmart' ),
					'plural'   => __( 'Virksomhedskoordinator', 'woodmart' ),
					'ajax'     => false,
				]
			);
		}

		/**
		 * Check is suborder.
		 */
		function is_suborder() {
			return isset( $_GET['sub_order'] ) && ! empty( $_GET['sub_order'] ) ? true : false;
		}

		/**
		 * Check is suborder.
		 */
		function get_company_id() {
			return isset( $_GET['company_id'] ) && ! empty( $_GET['company_id'] ) ? (int) $_GET['company_id'] : 0;
		}

		/**
		 * Returns the count of records in the database.
		 *
		 * @return null|string
		 */
		public static function record_count() {
			if ( self::is_suborder() ) {
				$user_count_query = array(
					'role' => 'customer',
					'meta_key' => '_company_id',
					'meta_value' => self::get_company_id(),
					'fields' => 'ids',
				);
				$count_users = get_users( $user_count_query );
				return count( $count_users );
			} else {
				$user_count = count_users();
				return isset( $user_count['avail_roles']['company_coordinator'] ) ? $user_count['avail_roles']['company_coordinator'] : 0;
			}

		}

		/**
		 * Text displayed when no customer data is available
		 */
		public function no_items() {
			_e( 'Record ikke fundet', 'woodmart' );
		}

		/**
		 * Render a column when no column specific method exist.
		 *
		 * @param array $item Data.
		 * @param string $column_name Column name.
		 *
		 * @return mixed
		 */
		public function column_default( $item, $column_name ) {
			// Get orders.
			$company_order = array(
				'numberposts' => 1,
				'post_type'   => wc_get_order_types( 'view-orders' ),
				'post_status' => array_keys( wc_get_order_statuses() ),
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => '_customer_user',
						'value' => $item->ID,
					),
					array(
						'key' => 'employee_csv_file',
						'value' => '',
						'compare' => '!=',
					),
				),
			);
			if ( $this->is_suborder() ) {
				$company_order['meta_query'] = array(
					array(
						'key' => '_customer_user',
						'value' => $item->ID,
					),
				);
			}
			$customer_orders = get_posts( $company_order );
			$customer_orders = reset( $customer_orders );
			// Display column.
			switch ( $column_name ) {
				case 'email':
					return $item->user_email;
				case 'phone':
					return get_user_meta( $item->ID, 'billing_phone', true );
				case 'order_id':

				if ( ! empty( $customer_orders ) ) {
					return '<a href="' . get_edit_post_link( $customer_orders->ID ). '" target="_blank">' . '#' . $customer_orders->ID . '</a>';
				} else {
					if ( $this->is_suborder() ) {
						return __( 'Incomplete', 'woodmart' );
					} else {
						return '-';
					}
				}
				case 'employee_order':
				if ( ! empty( $customer_orders ) ) {
					return '<a href="' . add_query_arg( array( 'page' => 'company-coordinator', 'sub_order' => $customer_orders->ID, 'company_id' => $item->ID ), admin_url( 'admin.php' ) ) . '">' . __( 'View', 'woodmart' ) . '</a>';
				} else {
					return '-';
				}
				default:
					return '';
			}
		}

		/**
		 * Render the bulk edit checkbox
		 *
		 * @param array $item
		 *
		 * @return string
		 */
		function column_cb( $item ) {
			return '';
		}

		/**
		 *  Associative array of columns
		 *
		 * @return array
		 */
		function get_columns() {
			$columns = [
				'name' => __( 'Name', 'woodmart' ),
				'email'    => __( 'Email', 'woodmart' ),
				'phone'    => __( 'Phone', 'woodmart' ),
				'order_id' => __( 'Order', 'woodmart' ),
				'employee_order' => __( 'medarbejderordre', 'woodmart' ),
			];
			if ( $this->is_suborder() ) {
				unset( $columns['employee_order'] );
			}
			return $columns;
		}


		/**
		 * Table extra table nav.
		 *
		 * @param string $which Which.
		 */
		function extra_tablenav( $which ) {
			if ( 'top' === $which && $this->is_suborder() ) : ?>
				<div class="alignleft actions tua-action">
					<a href="<?php echo esc_url( add_query_arg( 'page', 'company-coordinator', admin_url( 'admin.php' ) ) ); ?>" class="button button-primary"><?php _e( 'Back', 'woodmart' ); ?></a>
				</div>
			<?php endif;
		}

		/**
		 * Method for name column.
		 *
		 * @param array $item an array of DB data.
		 *
		 * @return string
		 */
		function column_name( $item ) {
			return $item->first_name;
		}

		/**
		 * Columns to make sortable.
		 *
		 * @return array
		 */
		public function get_sortable_columns() { return []; }

		/**
		 * Returns an associative array containing the bulk action
		 *
		 * @return array
		 */
		public function get_bulk_actions() { return []; }

		/**
		 * Handles data query and filter, sorting, and pagination.
		 */
		public function prepare_items() {
			$this->_column_headers = $this->get_column_info();
			$per_page     = $this->get_items_per_page( 'coordinator_per_page', 5 );
			$current_page = $this->get_pagenum();
			$total_items  = self::record_count();
			// Set pagination.
			$this->set_pagination_args(
				[
					'total_items' => $total_items,
					'per_page'    => $per_page,
				]
			);
			$user_query = array(
				'role' => 'company_coordinator',
				'number' => $per_page,
				'offset' => ( $current_page - 1 ) * $per_page,
			);
			if ( $this->is_suborder() ) {
				$user_query['role'] = 'customer';
				$user_query['meta_key'] = '_company_id';
				$user_query['meta_value'] = $this->get_company_id();
			}
			$this->items = get_users( $user_query );
		}
	}
}
