<?php
/**
 * Enqueue script and styles for child theme
 */
function woodmart_child_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'woodmart-style' ), woodmart_get_theme_info( 'Version' ) );
}
add_action( 'wp_enqueue_scripts', 'woodmart_child_enqueue_styles', 10010 );

// Customize WooCommerce.
require_once 'inc/woo-gift-voucher.php';

function custom_my_account_menu_items( $items ) {
    unset($items['downloads']);
    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'custom_my_account_menu_items' );

// To change add to cart text on single product page
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' ); 
function woocommerce_custom_single_add_to_cart_text() {
    return __( 'Vælg gave', 'woocommerce' ); 
}

// To change add to cart text on product archives(Collection) page
add_filter( 'woocommerce_product_add_to_cart_text', 'woocommerce_custom_product_add_to_cart_text' );  
function woocommerce_custom_product_add_to_cart_text() {
    return __( 'Vælg gave', 'woocommerce' );
}

// Easily do user specific CSS
function add_css_to_guest_user() {
    $guest_user = wp_get_current_user();
 
    if($guest_user && isset($guest_user->user_login) && 'guest' == $guest_user->user_login) {
        wp_enqueue_style( 'guest_admin_css', get_stylesheet_directory_uri() . '/css/wpadmin-guest.css' );
    }
}
add_action('admin_print_styles', 'add_css_to_guest_user' );

add_filter( 'woodmart_use_custom_price_widget', '__return_false', 10 );
add_filter( 'woodmart_use_custom_order_widget', '__return_false', 10 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

/**
 * Tilfældig rækkefølge
 */

add_filter( 'woocommerce_catalog_orderby', 'woo_random_sorting_option' );
 
function woo_random_sorting_option( $options ){
 
	$options['rand'] = 'Sorter tilfældig rækkefølge';
	return $options;
 
}

add_filter('woocommerce_default_catalog_orderby', 'misha_default_catalog_orderby');
 
function misha_default_catalog_orderby( $sort_by ) {
	return 'rand';
}

/**
 * Create single array to multidimensional.
 *
 * @param array $array Array Data.
 * @param int   $col_count Column count.
 * @return array.
 */
function woodmart_child_array_3d( $array, $col_count = 4 ) {
    $result = false;
    if( ! empty( $array ) && is_array( $array ) ) {
        $row_count = ceil( count( $array) / $col_count );
        $pointer = 0;
        for( $row=0; $row < $row_count; $row++ ) {
            for( $col=0; $col < $col_count; ++$col ) {
                if( isset( $array[$pointer] ) ) {
                    $result[$row][$col] = $array[$pointer];
                    $pointer++;
                }
            }
        }
    }
    return $result;
}

/**
 * Display vendor list.
 */
function display_vendors() {
$product_vendors = get_terms(
    array(
        'taxonomy' => 'wcpv_product_vendors',
        'hide_empty' => true,
    )
);
$product_vendors = woodmart_child_array_3d( $product_vendors );
ob_start();
if ( ! empty( $product_vendors) ) :
foreach( $product_vendors as $product_vendor ) :
?>
<div class="vc_row wpb_row vendor-content vc_row-fluid vc_custom_1602507857693 vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
    <?php
    foreach( $product_vendor as $vendor ) :
        $vendor_data = WC_Product_Vendors_Utils::get_vendor_data_by_id( $vendor->term_id );
        $user_data = get_user_by( 'email', $vendor_data['email'] );
        // $website = '';
        // $address = '';
        // if ( $user_data ) {
        //     $website = $user_data->user_url;
        //     $address = get_user_meta( $user_data->ID, 'billing_address_1', true );
        // }
    ?>
    <div class="wpb_column vc_column_container vc_col-sm-3">
        <div class="vc_column-inner vc_custom_1602507554305">
            <div class="wpb_wrapper">
                <div class="wpb_single_image wpb_content_element vc_align_center vc_custom_1602508116537">
                    <figure class="wpb_wrapper vc_figure">
                        <div class="vc_single_image-wrapper vc_box_border_grey">
                            <?php if ( $vendor_data['logo'] ) : ?>
                                <?php echo wp_get_attachment_image( $vendor_data['logo'], 'medium', '', array( 'class' => 'vendor-image' ) ); ?>
                            <?php else : ?>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/default.jpg" class="attachment-medium size-medium vendor-image" loading="lazy">
                            <?php endif; ?>
                        </div>
                    </figure>
                </div>

                <div class="wpb_text_column wpb_content_element">
                    <div class="wpb_wrapper vendor-info">
                        <p style="text-align: center;"><?php echo isset( $vendor_data['name'] ) ? $vendor_data['name'] : ''; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
</div>
<?php endforeach; endif;
$vendors = ob_get_clean();
return $vendors;
}
add_shortcode( 'display_vendor_list', 'display_vendors' );
